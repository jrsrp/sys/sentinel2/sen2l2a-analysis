% LaTeX document class for JRSRP report formatting
% This was my first attempt at writing a LaTeX class file, so I have probably made
% many stupid errors. However, mostly it seems to work. 
% I use the following resources in trying to work out the basics of a class file: 
%   https://www.sharelatex.com/blog/2011/03/27/how-to-write-a-latex-class-file-and-design-your-own-cv.html
%   https://www.tug.org/pracjourn/2006-4/flynn/flynn.pdf
%  /opt/sw/fw/rsc/SLES-11/texlive/2012/texmf-dist/doc/latex/base/clsguide.pdf
%
% The design was totally determined by the existing Word tempate document for JRSRP reports
% as created by Jo Edkins. I was just trying to mimic that. 
%


% Based it all on the default article class
\LoadClass{article}

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{jrsrpreport}[2015/05/27 Joint Remote Sensing Research Program Report class]

\RequirePackage{titlesec}
\RequirePackage{graphicx}
\RequirePackage{changepage}
\RequirePackage{hyphenat}


% Palatino default font
\renewcommand*\rmdefault{ppl}

\titleformat{\section}{\sffamily\Large\slshape}{\thesection}{1em}{}
\titleformat{\subsection}{\sffamily\large\slshape}{\thesubsection}{1em}{}

% Get the title page to come out right. Copied and hacked from the \def\@maketitle in article.cls. 
\let\@author\@empty
\def\@maketitle{%
  \newpage
  %\null
  % Organisational logo images
  \vspace*{-3cm}
  \begin{adjustwidth}{-2cm}{-3cm}
      \begin{tabularx}{1.5\textwidth}{XXX}
          \includegraphics[width=0.2\textwidth]{image1}&
            \includegraphics[width=0.1\textwidth]{image2}&
            \includegraphics[width=0.1\textwidth]{nsw}\\
          \includegraphics[width=0.17\textwidth]{vic}&
            \includegraphics[width=0.2\textwidth]{image5}&
            \includegraphics[width=0.1\textwidth]{une}\\
      \end{tabularx}
  \end{adjustwidth}
  
  \vskip 3cm
  \noindent\textbf{The Joint Remote Sensing Research Program}

  \noindent Publication Series

  \let \footnote \thanks
  \vskip 1cm
     {\Huge \noindent \nohyphens{\@title} \par}%
    % Save the title
    \global\let\mydoctitle\@title
    \vskip 3cm
    {
     \noindent \hangindent=4cm
%      \begin{tabular}[t]{l}
%       This only works as a non-tabular thing if the document does not use \and inside the \author
%       command. I don't really understand how to use \@author, but this kludge kind of works. 
        Author(s): \@author
%      \end{tabular}
      \par
    }%
    \vskip 0.7cm%
    \noindent {Date: \@date}%

  \par
  \vskip 0.7cm}

\renewenvironment{abstract}%
          {\noindent \hangindent=1.6cm Abstract:}%
          {\par}
 

% Page header
\RequirePackage{fancyhdr}
\fancyhead[C]{{\centering\mydoctitle}}
\fancyhead[R]{}
\fancyhead[L]{}
\fancyhfoffset[HLE,HRO]{6mm}
\pagestyle{fancy}
