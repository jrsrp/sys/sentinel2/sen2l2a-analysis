\documentclass[11pt]{jrsrpreport}
\usepackage[a4paper,left=2.5cm,right=2.5cm,bottom=2cm]{geometry}
\usepackage{textcomp}
\usepackage[natbib=true,citestyle=authoryear,bibstyle=authoryear,maxcitenames=1,
    maxbibnames=10,labeldateparts=true,uniquelist=minyear,urldate=long]{biblatex}
\addbibresource{sen2L2AcompareJRSRP.bib}
\usepackage{tabularx}
\usepackage{url}
\usepackage{placeins}
\usepackage{graphicx}
\usepackage[labelfont=bf, margin=10pt]{caption}
%\usepackage{draftwatermark}
%\SetWatermarkText{DRAFT}
%\SetWatermarkLightness{0.85}

\title{Comparing {S}entinel-2 surface reflectance from the {E}uropean {S}pace 
    {A}gency {L}evel {2A} processing and the {J}oint {R}emote {S}ensing {R}esearch 
    {P}rogram (Australia)}
\author{Neil Flood\textsuperscript{1, 2}\\
\begin{enumerate}
  \setlength{\itemindent}{1cm} \setlength{\itemsep}{-0.4em}
\item Joint Remote Sensing Research Program, University of Queensland
\item Qld Department of Environment and Science
\end{enumerate}
}

\begin{document}
\maketitle
\begin{abstract}
Pre-processed Sentinel-2 surface reflectance imagery is now available from 
the European Space Agency (ESA) as part of their 
Level 2A product. The current study samples this product across the continent of Australia, 
and compares with 
the same image dates, processed to surface reflectance using the methods of the 
Joint Remote Sensing Research Program (JRSRP). Comparison shows that there is quite good
agreement even without any adjustments. After correcting the ESA product for the effects
of the Bi-directional Reflectance Distribution Function (BRDF), in
a similar manner to that used in the standard JRSRP processing, the correlation between the two
datasets is significantly improved. A final adjustment is fitted to this corrected
data, to simulate JRSRP surface reflectance from ESA surface reflectance. With this in place, 
differences are generally within 1\%, and mean absolute differences (MAD) are generally 
between 0.005 and 0.010, with only the B11 MAD value reaching 0.015. This level of agreement
suggests that the ESA surface reflectance product, if adjusted for BRDF and systematic difference, 
could be used as a drop-in replacement for the JRSRP Sentinel-2 surface reflectance. 
\end{abstract}

\newpage\tableofcontents\newpage

\section{Introduction}
\label{sec:introduction}

Since the European Space Agency (ESA) launched the first of its Sentinel-2 satellites
\citep{Drusch2012}, 
in June 2015, it has been distributing the data processed to Level 1C (L1C), which is 
a top-of-atmosphere reflectance. At around the same time, they also made available a 
prototype of their processing software (called sen2cor) to further process this to 
Level 2A (L2A), a surface reflectance product, i.e. corrected for atmospheric effects. 

Within Australia, the Joint Remote Sensing Research Program (JRSRP) is a collaboration between 
a number of state government agencies and universities, and has a long history of working in
applications of satellite imagery for earth obesrvation, including radiometric processing
of Landsat and SPOT imagery. The JRSRP carried out some prelimary testing of the 
sen2cor corrections in that early prototype, and found that the software was not 
yet mature, and not suitable for continuity with existing JRSRP work. For this reason, 
the JRSRP chose to extend its existing surface reflectance processing for Landsat and SPOT, 
described by \citet{Flood2013}, to also include Sentinel-2. Tests showed that this gave a suitable 
product for use with existing JRSRP modelling applications~\citep{Flood2017}. 

In December 2018, ESA began distributing pre-calculated L2A imagery, for the whole globe, 
from their Scihub distribution point~\citep{Scihub}. More recent testing of the sen2cor
framework by others suggests that it rapidly improved from the initial 
version~\citep{Vuolo2016}. At the time of writing, there is now
a year of such data available, and being distributed within Australia by the Australiasian
regional hub~\citep{AuscophubSARA}. 

The present study carries out a comparison of the new Sentinel-2 L2A surface reflectance
product with the existing JRSRP Sentinel-2 surface reflectance, to assess its suitability 
for use with existing JRSRP applications, and/or as a basis for the development of 
future applications. 

\section{Data}
\label{sec:data}

All Level 2A imagery was downloaded from Scihub~\citep{Scihub}, via the Australasian Copernicus 
Hub~\citep{AuscophubSARA}, using the SARA client code~\citep{AuscophubClient}. 
Searches were performed in early January 2020, using a simple area of interest polygon
surrounding Australia. The search was restricted to dates before the end of
August 2019, in order to avoid including data from the extreme bush fires
prevalent across much of eastern Australia during the later part of 2019. The range
of image dates selected was from December 2018 to August 2019. 

The version number of the ESA processing software used to produce the Level 2A product was
either 02.11, 02.12 or 02.13, depending on the imagery date. 

Selection and extraction of points was performed using custom Python code. See Appendix
for details on availability of the code. Also included in that repository are text files
listing exactly which image pairs were selected. 

The dates found in the SARA search were matched with equivalent information from RSC's
database of local holdings. The JRSRP hold and process data for the Australian states of
Queensland, New South Wales, Victoria, Tasmania and Northern Territory. Dates present in 
both were accepted as usable for the comparison exercise. Dates with only a small part of 
the whole scene acquired were excluded, to avoid sampling from small slivers. For each included
scene, one date was randomly selected from the cloud-free dates available during the chosen 
time period. 

The JRSRP processing uses the methods described by \citet{Flood2013}, supplemented by the 
inclusion of the relative spectral response functions for Sentinel-2A and Sentinel-2B, as 
supplied by ESA~\citep{ESA_SRF}. The same imagery has been processed to surface reflectance
using these methods. 

From these image pairs, points were selected from a regular grid with a spacing of 10~km 
in north-south and east-west directions. 

In selecting individual points, the Scene Classification Layer of the Level 2A product was also
used to remove points affected by cloud or shadow. Points were only included if the imagery was
non-null in all bands, and if the Scene Classification Layer had a class of either 4, 5 or 7, 
corresponding to classes VEGETATION, NON\_VEGETATED or UNCLASSIFIED. This excludes cloud, 
shadow, water and snow (\citep{l2aAlgOverview}, figure 3, "Scene Classification Values"). 

Care was taken to ensure a roughly even geographical spread of points across the JRSRP
area of interest, and an even spread of dates through the chosen time period of interest. 

The selected points are shown as dots in figure~\ref{fig:refcomparepointsmap}. 
A total of 67220 points were included. 

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{refcomparepointsmap}
  \caption{Locations of points use in comparison. Each blue dot is a single point. 
    Note that the white lines across parts of the blue area are Moir\'e patterns, 
    which disappear at different zoom levels, and do not represent genuine gaps in the
    sampling. }
  \label{fig:refcomparepointsmap}
\end{figure}

\section{Methods}
\label{sec:methods}

At the set of points generated across all selected imagery (see section~\ref{sec:data}), the pixel 
values are queried from the imagery for both the ESA Level 2A and JRSRP surface reflectance. 
This gives a set of pairs of surface reflectance estimate for points across the JRSRP
region of interest (figure~\ref{fig:refcomparepointsmap}). All 10 of the surface reflectance 
bands were included in analyses and model fitting. 

Scatter density plots of the paired reflectance values are used to compare the values over
the whole dataset. Only the 6 bands equivalent to Landsat Thematic Mapper (TM) bands are
presented here, with the red edge bands being omitted for the sake of brevity.

On each plot, three measures of the relationship are included. Firstly, we use orthogonal distance 
regression (ODR) to calculate a slope of the linear relationship (forced through the origin). 
This gives a rough measure 
of the ratio of the reflectances, indicating whether one is systematically higher or lower than the
other, and by what proportion. Secondly, we calculate the mean absolute difference (MAD) between 
reflectance values. This is a measure of how large a difference in reflectance values 
one sees between the two products. 
Thirdly, we calculate the linear correlation coefficient ($r$). This is essentially a
measure of the scatter on the scatter plots, independent of the systematic average relationships
measured by the ODR slope and the MAD. 

A perfect match in reflectance values, with no difference at all, would have ODR slope of 1.0, 
MAD of zero, and $r=1$. 

As discussed by \citet{Flood2020}, the ESA surface reflectance product does not include
any correction for the effects of the Bi-directional Reflectance Distribution 
Function (BRDF). By contrast, the JRSRP processing includes a BRDF standardization, 
adjusting reflectances to a standard set of sun and satellite angles~\citep{Flood2013}. 
The BRDF modelling by \citet{Flood2020} can be applied directly to the ESA Level 2A
product, and is used here to apply an equivalent reflectance adjustment to the
same standard angles used for the JRSRP product. 

Even after correcting for BRDF effects, there remain other differences between the
two products. With this in mind, a final adjust is fitted, for each band, which characterizes
all remaining difference, and allows adjustment of the ESA Level 2A product to estimate
the JRSRP surface reflectance values. This adjustment is a simple ordinary least squares
linear regression, independently for each band (equation~\ref{eq:refESA_to_refJRSRP}). 

\begin{eqnarray}
\label{eq:refESA_to_refJRSRP}
\rho_J = K_0 + K_1 \rho_E
\end{eqnarray}

where $\rho_E$ is the reflectance from ESA, after BRDF standardization, and $\rho_J$ is 
the estimate of reflectance from the JRSRP product. $K_0$ and $K_1$ are the regression 
coefficients (per band). 

Three sets of scatter plots are generated. The first shows the direct relationship 
between the unmodified surface reflectance values for each of the two products. The second
shows the relationship after adjusting the ESA product for BRDF effects. 
The third set shows the relationship after adjusting the ESA values for BRDF effects and 
then adjusting them to estimate JRSRP reflectance. This final transformed surface reflectance 
is intended to represent how the ESA Level 2A product might be used in contexts which 
require compatibility with the existing JRSRP product, such as being used as input to an
existing JRSRP model. 

\section{Results}
\label{sec:results}

Figure~\ref{fig:refcompare} shows fairly good agreement between the two products, even with no
adjustments at all. The correlation values range between 0.93 and 0.98, depending on the band. 
The mean absolute difference values are bwteeen 0.009 and 0.035. 
The systematic bias measured by the ODR slope values are between 5 and 13\%, depending 
on the band, and show that in all bands, the ESA reflectance is slightly larger than the JRSRP
values. 

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.9\textwidth]{refcompare}
  \caption{Surface reflectance from JRSRP and ESA L2A. X axis is surface reflectance from 
    JRSRP, Y axis is from ESA L2A. No adjustments performed. }
  \label{fig:refcompare}
\end{figure}

Figure~\ref{fig:refcompare_brdfadj} shows an equivalent set of scatter plots, after adjusting
the ESA reflectance values for BRDF effects by standardizing to the same angles as the 
JRSRP product. In all bands, all three measures of agreement are improved. The ODR slope values 
are all appreciably closer to 1.0, in the range 0.98--1.09. The MAD values are all reduced, 
and now range between 0.006 and 0.022. The correlation values are all increased, and are 
now between 0.07 and 0.99 for all bands. 
\begin{figure}[ht]
  \centering
  \includegraphics[width=0.9\textwidth]{refcompare_brdfadj}
  \caption{Surface reflectance from JRSRP and ESA L2A. X axis is surface reflectance from 
    JRSRP, Y axis is from ESA L2A. Modelling of BRDF has been used to adjust ESA reflectance 
    to the JRSRP standard angles. }
  \label{fig:refcompare_brdfadj}
\end{figure}

The fitted coefficients for use with equation~\ref{eq:refESA_to_refJRSRP} are given in 
table~\ref{tbl:jrsrpadjustparams}. 

\begin{table}
\centering
\input{jrsrpadjustparams.tex}
\caption{Fitted parameters for the linear adjustment equation to account for remaining
    differences between the two reflectance products (after BRDF standardization). 
    Suitable for use in equation~\ref{eq:refESA_to_refJRSRP}}
\label{tbl:jrsrpadjustparams}
\end{table}

Figure~\ref{fig:refcompare_brdfadj_fudged} shows another equivalent set of scatter plots, 
after adjusting the BRDF-corrected ESA reflectance values to more closely match the
JRSRP values. These plots are using the same complete dataset which was also used to fit
the coefficients of this adjustment. They show very close agreement between the two products, 
with mean absolute difference values reduced to the range 0.005--0.015. 

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.9\textwidth]{refcompare_brdfadj_fudged}
  \caption{Surface reflectance from JRSRP and ESA L2A. X axis is surface reflectance from 
    JRSRP, Y axis is from ESA L2A. Modelling of BRDF has been used to adjust ESA reflectance 
    to the JRSRP standard angles. A final linear adjustment has been used to 
    adjust ESA reflectance to best match JRSRP. }
  \label{fig:refcompare_brdfadj_fudged}
\end{figure}

While the scatter plots for the red edge bands are omitted for the sake of brevity, they show
very similar behaviour to the bands shown here, and the various metrics are within the ranges
quoted above. 

\section{Discussion}
\label{sec:discussion}

The good agreement seen between the JRSRP surface reflectance product and the undmodified
ESA Level 2A product, as shown in figure~\ref{fig:refcompare}, is very encouraging. The 
values are strongly correlated, and are certainly of the same rough magnitude. This is not
altogether surprising, as ~\citet{Vuolo2016} have shown that the ESA product matches well with
similar Landsat products, and \citet{Flood2017} has shown that the JRSRP product also matches 
well with Landsat products processed to JRSRP specifications, so one might expect a fairly good
agreement. 

As discussed by \citet{Flood2020}, the addition of the BRDF standardization can remove a notable 
amount of variation in the data which is unconnected to change on the ground. Given that the 
JRSRP processing already applies an equivalent standardization to their Sentinel-2 
surface reflectance, it is also unsurprising that applying it to the ESA product 
will improve the agreement between the two. In going from figure~\ref{fig:refcompare}
to figure~\ref{fig:refcompare_brdfadj}, the improvements in all measures of agreement 
provide additional support for the suggested benefit of using this technique. 

After applying the BRDF standardization, the remaining differences between the two products
probably result from complex and subtle differences in the atmospheric correction 
methods used. Whilst one or the other may (or may not) be more accurate, in an 
absolute sense, this cannot be determined just by comparing these two datasets. 
On the other hand, of far more importance is the consistency of reflectance data, 
both spatial and temporal, rather than its absolute accuracy. Much of the power of 
satellite imagery is dependent on relating it to field data of some sort, and 
the JRSRP has a long history of collecting ground-level data on bio-physical quantities
such as vegetation cover. Given the expense of these field datasets, it is vital
that data collected long before the launch of Sentinel-2 is still usable. As
discussed by \citet{Flood2014} and \citet{Flood2017}, this allows 
existing models which estimate bio-physical quantities like Foliage Projective Cover (FPC) 
and fractional vegetation cover as a function of reflectance data to be used 
seamlessly with Sentinel-2 data. 

The final linear adjustment applied to the BRDF-standardized ESA data results in very good
agreement with the existing JRSRP Sentinel-2 surface reflectance data, as seen in 
figure~\ref{fig:refcompare_brdfadj_fudged}. The ODR slope values are all 1.00, except for the
blue band, where it is 0.99, showing that there is negligible systematic bias, either higher
or lower, between the two products. The correlation values are all in the range 0.97--0.99, 
i.e. very close to 1.0. The MAD values for all bands lie in the range 0.005--0.015, with only
one band (B11) exceeding 0.010. 

Only limited work has yet been done on the sensitivity of the existing JRSRP models to
variation in reflectance values, and as yet none of it has been published or 
widely disseminated. However, the unpublished work which has been done suggests that 
the major limits in predicting bio-physical quantities such as cover variables now lie
in the models themselves, and the non-uniqueness of the relationships with reflectance, 
rather than in the accuracy of the reflectance inputs. 

\section{Conclusion and Recommendations}
\label{sec:conclusion}

The ESA Level 2A surface reflectance product is broadly comparable with the equivalent
product from the Joint Remote Sensing Research Program (JRSRP). Overall, the ESA values 
are 5--13\% higher than those of JRSRP, depending on the band, and the values are strongly 
correlated. 

After correcting for BRDF effects by adjusting to the same standard angles as the JRSRP 
currently use \citep{Flood2020}, the correlations between the products are 
increased to around 0.98. 

By fitting an empirical linear adjustment to compensate for differences in the atmospheric 
correction models used, the systematic bias between the two products is removed, with neither
one being systematically higher or lower than the other, and the mean absolute 
differences are in the range 0.005--0.012, depending on the band. 

With the use of both the BRDF standardization and the empirical linear adjustment, the ESA
surface reflectance product would be quite adequate as a drop-in replacement for the 
current JRSRP Sentinel-2 surface reflectance product. 

As discussed by \citet{Flood2020}, it is recommended that the BRDF standardization be applied
whenever the ESA product is used for modelling calculations or classification. It is further 
recommended that the linear JRSRP adjustment be used, but {\em only} when compatibility 
with historic JRSRP work is required, such as when using an existing model which 
was parameterized using the existing JRSRP surface reflectance. It is recommended that all 
modelling and classification work be developed using the ESA Level 2A surface reflectance
product, with the addition of the BRDF-standardization. 

In moving to use the ESA product, this will greatly reduce the need for JRSRP to maintain in-house
expertise on radiometric corrections. 

\FloatBarrier
\section*{Appendix}
All source code for data selection and analysis is available in a git repository 
hosted by the Joint Remote Sensing Research Program (JRSRP), at 
\url{https://gitlab.com/jrsrp/sys/sen2l2a.git}. Included in that repository are the 
text files listing which image pairs were selected, and the curl commands to download
the Level 2A zip files from the Australian Copernicus Hub SARA server. 

\printbibliography

\end{document}
