"""
Utilities for the BRDF comparison. 
"""
from __future__ import print_function, division

import numpy
import pandas
import pylab
import matplotlib
from scipy import odr
from scipy.stats import pearsonr

# All 10m and 20m bands
bandsToProcess = ['B02', 'B03', 'B04', 'B05', 'B06', 'B07', 
    'B08', 'B8A', 'B11', 'B12']
bands10m = ('B02', 'B03', 'B04', 'B08')
bands20m = ('B05', 'B06', 'B07', 'B8A', 'B11', 'B12')

# Symbols for different ordering variables. 
ORDER_SATAZ = "sataz"
ORDER_SUNZEN = "sunzen"
ORDER_UNCHANGED = "unchanged"

greyColourMap = matplotlib.colors.LinearSegmentedColormap.from_list('MyGrey', 
    [[0.8,0.8,0.8],[0.05,0.05,0.05]], N=20)


def readBRDFpointfile(filename):
    """
    Read the point file output by extractPairedReflectance.py
    Return a Pandas Dataframe. 
    """
    df = pandas.read_csv(filename, delim_whitespace=True)
    
    # If the blue band varies by a factor of 2 in either direction, then
    # this pixel is probably undetected cloud or shadow, and should be removed. 
    blueVariationRatio = (df['B02_ref1'] / df['B02_ref2'])
    acceptableVariation = ((blueVariationRatio > 0.5) & (blueVariationRatio < 2))
    df = df[acceptableVariation]
    
    return df


def plotScatter(x, y, plotTitle):
    """
    Plot a single density scatter plot of the two reflectance variables. Intended
    to be used as a subplot in a larger set of plots. 
    """
    # Discard extreme outliers so they don't interfere with the plot bounds
    ref99 = max(numpy.percentile(x, 99.9), numpy.percentile(y, 99.9))
    keep = ((x <= ref99) & (y <= ref99))
    x = x[keep]
    y = y[keep]
    
    pylab.hexbin(x, y, mincnt=1, cmap=greyColourMap)
    
    # Set a neat value for the upper bound on the axes
    refMax = max(x.max(), y.max())
    upperBounds = numpy.array([0.2, 0.4, 0.6, 0.8, 1.0])
    boundNdx = numpy.digitize(refMax, upperBounds).clip(0, len(upperBounds)-1)
    refMax = upperBounds[boundNdx]

    # Plot 1-to-1 line
    pylab.plot([0, refMax], [0, refMax], color='k')
    pylab.title(plotTitle)
    
    # Do a few statistics
    p = fitODR(x, y)
    pylab.text((0.05 * refMax), (0.9 * refMax), "$y={:.2f}x$ (ODR)".format(p))
    mad = numpy.abs(x-y).mean()
    pylab.text((0.05 * refMax), (0.83 * refMax), "MAD$={:.3f}$".format(mad))
    (rval, pval) = pearsonr(x, y)
    pylab.text((0.05 * refMax), (0.76 * refMax), "$r={:.2f}$".format(rval))
    
    # Plot ODR regression line
    lineMax = min(refMax * p, refMax / p)
    pylab.plot([0, lineMax], [0, lineMax*p], color='gray', linestyle='--')

    # Reset axis limits    
    pylab.xlim(0, refMax)
    pylab.ylim(0, refMax)


def fitODR(x, y):
    """
    Fit an orthogonal distance regression, forced through the origin. 
    Return the slope value. 
    """
    def fn(p, x):
        return (p[0] * x)

    mdl = odr.Model(fn)
    data = odr.Data(x, y)
    reg = odr.ODR(data, mdl, beta0=[1.0])
    reg.run()
    return reg.output.beta[0]

def plotScatterGroup(xyList, varnameList, plotfile):
    """
    Plot a list of xy pairs as a group of scatter density plots in a single
    figure. 
    """
    numPlots = len(xyList)
    if numPlots == 6:
        (numPlotsX, numPlotsY) = (3, 2)
        pylab.figure(figsize=(9, 6))
    elif numPlots == 4:
        (numPlotsX, numPlotsY) = (2, 2)
        pylab.figure(figsize=(6, 6))
    pylab.subplots_adjust(left=0.06, right=0.97, bottom=0.05, top=0.95)

    for i in range(numPlots):
        pylab.subplot(numPlotsY, numPlotsX, (i+1))
        (x, y) = xyList[i]
        varName = varnameList[i]
        plotScatter(x, y, varName)
    
    if plotfile is not None:
        pylab.savefig(plotfile)
    else:
        pylab.show()


def orderRefBySelector(refPair, selector):
    """
    Sorts the reflectance pairs, point by point, according to the selector
    array. The refPair array has two columns, with each row being a pair of 
    reflectance values. The return is a similar 2-d arrays, but the order 
    of each pair has been (potentially) changed. Where selector is 0, the 
    order of the pair is unchanged, but where selector is 1, the order of the pair
    is reversed. 
    """
    # I feel like I ought to be able to do this with direct numpy indexing,
    # but can't figure out how. So, this list comprehension is a less efficient
    # but practical compromise. 
    orderedRefPair = numpy.array([refPair[i] if selector[i] == 0 else refPair[i][::-1] 
        for i in range(len(selector))])
    return orderedRefPair


def calcBRDFkernelsForBand(data, bandIdStr, pairSelector):
    """
    Given a dataframe and a bandIdStr, return a tuple of the two BRDF kernel
    functions (Kgeo, Kvol), for the chosen half of the pair.
    
    pairSelector is either 1 or 2, and selects the corresponding member of the pair
    """
    sunAz = numpy.radians(data['sunAz{}'.format(pairSelector)].values)
    sunZen = numpy.radians(data['sunZen{}'.format(pairSelector)].values)
    satAz = numpy.radians(data['{}_satAz{}'.format(bandIdStr, pairSelector)].values)
    satZen = numpy.radians(data['{}_satZen{}'.format(bandIdStr, pairSelector)].values)
    
    relAz = (satAz - sunAz)
    Kgeo = calcKgeo(sunZen, satZen, relAz)
    Kvol = calcKvol(sunZen, satZen, relAz)
    return (Kgeo, Kvol)


def rtls_n(p, Kgeo, Kvol):
    """
    Normalized RossThickLiSparse function, The 'iso' parameter is assumed to be
    1 (i.e. we have normalized), and the other two parameters 'geo' and 'vol' 
    are given in the p array. 
    
    The two kernel functions Kgeo and Kvol have been pre-calculated, as these 
    are a function only of the angles. 
    
    """
    ref = (1 + p[0] * Kgeo + p[1] * Kvol)
    return ref


def calcKgeo(sunZen, satZen, relAz):
    """
    Calculate the BRDF 'geo' kernel, Li-Sparse. Zenith angles are assumed to be 
    non-negative. All angles are in radians. 

    The equations were adopted from MODIS BRDF/Albedo Product: Algorithm 
    Theoretical Basis Document v 5.0

    Fixed parameters br and hb are the b/r and h/b ratios specified 
    in the ATBD (page 14). They represent the crown shape and crown height
    respectively. The values given for these are those specified in 
    the ATBD for the MODIS global processing. 
    """
    br = 1
    hb = 2
    sunZenAdj = numpy.arctan(br*numpy.tan(sunZen))
    satZenAdj = numpy.arctan(br*numpy.tan(satZen))
    phaseAngle = numpy.arccos((numpy.cos(sunZenAdj)*numpy.cos(satZenAdj)) + 
        (numpy.sin(sunZenAdj)*numpy.sin(satZenAdj)*numpy.cos(relAz)))
    D = numpy.sqrt((numpy.tan(sunZenAdj))**2 + (numpy.tan(satZenAdj))**2 -
        (2*numpy.tan(sunZenAdj)*numpy.tan(satZenAdj)*numpy.cos(relAz)))
    cos_t = hb*((numpy.sqrt(D**2 + (numpy.tan(sunZenAdj)*numpy.tan(satZenAdj)*numpy.sin(relAz))**2)) / 
        ((1/numpy.cos(sunZenAdj))+(1/numpy.cos(satZenAdj))))
    cos_t = numpy.clip(cos_t, -1, 1)
    t = numpy.arccos(cos_t)
    O = 1/numpy.pi * ((t-(numpy.sin(t)*numpy.cos(t))) * ((1/numpy.cos(sunZenAdj))+(1/numpy.cos(satZenAdj))))
    Kgeo = (O - (1/numpy.cos(sunZenAdj)) - (1/numpy.cos(satZenAdj)) + 
        (0.5*(1+numpy.cos(phaseAngle))*(1/numpy.cos(sunZenAdj))*(1/numpy.cos(satZenAdj))))
    return Kgeo


def calcKvol(sunZen, satZen, relAz):
    """
    Calculate the BRDF 'vol' kernel, RossThick. Zenith angles are assumed to be 
    non-negative. All angles are in radians. 

    The equations were adopted from MODIS BRDF/Albedo Product: Algorithm 
    Theoretical Basis Document v 5.0
    """
    phaseAngle=numpy.arccos((numpy.cos(sunZen)*numpy.cos(satZen)) + 
        (numpy.sin(sunZen)*numpy.sin(satZen)*numpy.cos(relAz)))
    Kvol=(((((numpy.pi/2) - phaseAngle) * numpy.cos(phaseAngle)) + 
        numpy.sin(phaseAngle)) / (numpy.cos(sunZen) + numpy.cos(satZen))) - (numpy.pi/4)
    return Kvol


