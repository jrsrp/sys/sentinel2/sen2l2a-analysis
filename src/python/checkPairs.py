#!/usr/bin/env python
"""
For a list of pairs of zip files, do a quick reality check
on each pair. Checks the ratio of the average reflectance 
across the overlap area. The ratio is always calculated with the
larger value on top, so that values should be >= 1. 

Outputs an ordered list of the ratios, with the associated zip file pair
(largest first). 

The idea is that we can quickly detect cases of radical change
between the two dates, e.g. due to undetected cloud cover, 
or major on-ground change like fire scar. 

"""
from __future__ import print_function, division

import os
import argparse

from osgeo import gdal

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--zipfilepairs", help="Text file of pairs of zip files")
    p.add_argument("--outfile", help="Output text file of pair ratios, in descending order")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    pairlist = readZipPairs(cmdargs.zipfilepairs)
    
    ratioList = [compareZipfiles(zip1, zip2) for (zip1, zip2) in pairlist]
    ratioList = [r for r in ratioList if r is not None]
    ratioList = sorted(ratioList, reverse=True)
    
    writeRatioList(ratioList, cmdargs.outfile)


def compareZipfiles(zip1, zip2):
    """
    Compare the two given zip files. Return a tuple
        (ratio, (zip1, zip2))
    where ratio is the ratio of the two blue bands, over the 
    intersecting non-null area, with the larger value always on top, 
    i.e. ratio >= 1
    
    """
    blue1 = getBlue(zip1)
    blue2 = getBlue(zip2)
    # Assume that the null value is known and always the same
    NULLVAL_DN = 0
    nonNull = ((blue1 != NULLVAL_DN) & (blue2 != NULLVAL_DN))
    avgBlue1 = blue1[nonNull].mean()
    avgBlue2 = blue2[nonNull].mean()

    if avgBlue1 > avgBlue2:
        ratio = avgBlue1 / avgBlue2
    else:
        ratio = avgBlue2 / avgBlue1
    return (ratio, (zip1, zip2))


def getBlue(zipfn):
    """
    Given the SAFE zipfile, read the 10m blue band as an array. Uses GDAL's
    native SENTINEL2 driver to do the work. 
    """
    zipds = gdal.Open(zipfn)
    subdsList = zipds.GetSubDatasets()
    dsName10m = subdsList[0][0]
    ds10m = gdal.Open(dsName10m)
    blueBand = ds10m.GetRasterBand(1)
    blueArr = blueBand.ReadAsArray()
    return blueArr


def readZipPairs(zipfilepairs):
    """
    Read the list of pairs of zip files
    """
    pairlist = [line.strip().split() for line in open(zipfilepairs)]
    # Remove any pair which is missing either of its zip files
    pairlist = [pair for pair in pairlist if os.path.exists(pair[0]) and os.path.exists(pair[1])]

    return pairlist


def writeRatioList(ratioList, outfile):
    """
    Write the list of ratios and zip file pairs
    """
    f = open(outfile, 'w')
    for (ratio, (zip1, zip2)) in ratioList:
        outline = "{} {} {}".format(ratio, zip1, zip2)
        print(outline, file=f)
    f.close()


if __name__ == "__main__":
    main()
