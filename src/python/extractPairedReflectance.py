#!/usr/bin/env python
"""
Given a list of zipfile pairs, extract paired reflectance values for a sample
of points across the overlap area in each pair.

Output text file of sampled reflectance values from the pairs, along with 
associated information like sun and view angles from each side of the pair,
point locations and dates. 

"""
from __future__ import print_function, division

import os
import argparse
import zipfile

import numpy

import brdfutils
import extractionutils


def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--zipfilepairs", help="Text file of pairs of zip files")
    p.add_argument("--zipfiledir", help="Directory in which all zip files are found")
    p.add_argument("--outfile", help="Text file of point reflectances, sampled from image overlaps")
    p.add_argument("--samplestep", type=int, default=10000, 
        help="Sample step size in each direction (metres) (default=%(default)s)")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    pairlist = readZipPairs(cmdargs)
    extractAllData(pairlist, cmdargs)


def extractAllData(pairlist, cmdargs):
    """
    Extract data from all pairs of zip files
    """
    outf = createOutfile(cmdargs)
    
    count = 1
    for (zip1, zip2) in pairlist:
        print("Pair", count, "of", len(pairlist))
        try:
            extractPair(zip1, zip2, cmdargs.samplestep, outf)
        except zipfile.BadZipfile:
            print("  Skipping. Bad zip file")
        count += 1
    
    outf.close()


def extractPair(zip1, zip2, sampleStep, outf):
    """
    Extract all data for a given pair of zip files
    
    Discards pixels if any band is null, if any angle value is null, and if the 
    Scene Classification layer (SCL) says it is not land surface. This latter excludes
    any cloud, shadow, water and snow. 
    """
    (zipmeta1, granulemeta1) = extractionutils.readMetadata(zip1)
    (zipmeta2, granulemeta2) = extractionutils.readMetadata(zip2)
    
    pointsXY = extractionutils.generatePoints(granulemeta1, sampleStep)
    bandObjDict1 = extractionutils.openAllBands(zip1)
    bandObjDict2 = extractionutils.openAllBands(zip2)
    
    tileId = granulemeta1.tileId
    date1 = granulemeta1.datetime.strftime("%Y-%m-%d")
    date2 = granulemeta2.datetime.strftime("%Y-%m-%d")
    
    # The SCL codes which we will accept as valid land surface codes. Accepting only
    # VEGETATION, NOT_VEGETATED and UNCLASSIFIED. Code values from Figure 3 of 
    # https://sentinel.esa.int/web/sentinel/technical-guides/sentinel-2-msi/level-2a/algorithm
    sclCodesLandSfc = set([4, 5, 7])
    
    numPts = len(pointsXY)
    for i in range(numPts):
        (x, y) = tuple(pointsXY[i])
        sclValue1 = extractionutils.queryPix(bandObjDict1, x, y, 'SCL')
        sclValue2 = extractionutils.queryPix(bandObjDict2, x, y, 'SCL')
        if sclValue1 in sclCodesLandSfc and sclValue2 in sclCodesLandSfc:
            (sunAz1, sunZen1) = extractionutils.querySunAngles(granulemeta1, x, y)
            (sunAz2, sunZen2) = extractionutils.querySunAngles(granulemeta2, x, y)

            # Assemble a list of the values for this point. Note the correspondence 
            # with the headings assembled in writeOutfile()
            pointVals = [x, y, tileId, date1, date2, sunAz1, sunZen1, sunAz2, sunZen2]

            allValsOK = True
            for bandIdStr in brdfutils.bandsToProcess:
                (satAz1, satZen1) = extractionutils.querySatAngles(granulemeta1, x, y, bandIdStr)
                (satAz2, satZen2) = extractionutils.querySatAngles(granulemeta2, x, y, bandIdStr)
                ref1 = extractionutils.queryPix(bandObjDict1, x, y, bandIdStr)
                ref2 = extractionutils.queryPix(bandObjDict2, x, y, bandIdStr)

                # Add on the columns of data for this band. Note the correspondence 
                # with the column headings assembled in writeOutfile()
                ref1scaled = round(ref1 / zipmeta1.scaleValue, 4)
                ref2scaled = round(ref2 / zipmeta2.scaleValue, 4)
                bandVals = [ref1scaled, satAz1, satZen1, ref2scaled, satAz2, satZen2]
                pointVals.extend(bandVals)

                refOK = ((ref1 != zipmeta1.nullVal) and (ref2 != zipmeta2.nullVal))
                anglesOK = all([not numpy.isnan(angle) for angle in [satAz1, satZen1, satAz2, satZen2]])
                allValsOK = (allValsOK and refOK and anglesOK)

            if allValsOK:
                writeOutputLine(outf, pointVals)


def readZipPairs(cmdargs):
    """
    Read the list of pairs of zip files
    """
    pairlist = [line.strip().split() for line in open(cmdargs.zipfilepairs)]
    # Add the directory
    pairlist = [[os.path.join(cmdargs.zipfiledir, fn) for fn in pair] for pair in pairlist]
    # Remove pairs with one or more missing zip files
    pairlist = [pair for pair in pairlist if os.path.exists(pair[0]) and os.path.exists(pair[1])]
    return pairlist


def createOutfile(cmdargs):
    """
    Write a text file (space-separated) of the full output data. Includes a
    line of column headings at the top. 
    """
    # Assemble the column headings. 
    headings = ['Easting', 'Northing', 'Tile', 'Date1', 'Date2', 
            'sunAz1', 'sunZen1', 'sunAz2', 'sunZen2']
    for bandIdStr in brdfutils.bandsToProcess:
        bandHeadings = ["{}_{}".format(bandIdStr, col)
            for col in ['ref1', 'satAz1', 'satZen1', 'ref2', 'satAz2', 'satZen2']]
        headings.extend(bandHeadings)
    
    f = open(cmdargs.outfile, 'w')
    print(' '.join(headings), file=f)
    return f


def writeOutputLine(f, dataLine):
    """
    Write a single row of output to the given output file
    """
    print(' '.join([str(v) for v in dataLine]), file=f)


if __name__ == "__main__":
    main()
