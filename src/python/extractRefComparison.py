#!/usr/bin/env python
"""
Extract paired reflectance data from pairings of ESA vs JRSRP processed surface reflectance. 

Each pair consists of 1 ESA L2A zipfile and the two JRSRP QVF files of surface reflectance 
(aba and abb stages). A grid of points of sampled across these files, and the pairs of 
reflectance values extracted for each band. 

Output it written to a simple text file, space-separated. 

"""
from __future__ import print_function, division

import os
import argparse
import zipfile

import numpy
from osgeo import gdal

import qvf

import brdfutils
import extractionutils

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--pairsfile", help="File listing pairings")
    p.add_argument("--datadir", help="Directory containing data files")
    p.add_argument("--outfile", help="Output text file")
    p.add_argument("--samplestep", type=int, default=10000, 
        help="Sample step size in each direction (metres) (default=%(default)s)")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    pairlist = [line.strip().split() for line in open(cmdargs.pairsfile)]
    
    outf = createOutfile(cmdargs)
    
    count = 1
    for (abaname, zipname) in pairlist:
        print("Pair", count, "of", len(pairlist))
        try:
            extractPair(abaname, zipname, cmdargs, outf)
        except zipfile.BadZipfile:
            print("  Skipping. Bad zip file")
        except IOError:
            print("  Skipping. Missing file")
        count += 1
    
    outf.close()


def extractPair(abaname, zipname, cmdargs, outf):
    """
    Extract point data for a single pair of files, and write it to 
    the given outfile
    """
    zipnameFull = os.path.join(cmdargs.datadir, zipname)
    abanameFull = os.path.join(cmdargs.datadir, abaname)
    
    (zipmeta, granulemeta) = extractionutils.readMetadata(zipnameFull)
    
    pointsXY = extractionutils.generatePoints(granulemeta, cmdargs.samplestep)
    bandObjDictESA = extractionutils.openAllBands(zipnameFull)
    bandObjDictJRSRP = openAllJRSRPbands(abanameFull)
    
    # I could get this out using the DNscaling object, but can't be bothered
    refScaleJRSRP = 10000
    refNullJRSRP = 0

    tileId = granulemeta.tileId
    date = granulemeta.datetime.strftime("%Y-%m-%d")
    
    # The SCL codes which we will accept as valid land surface codes. Accepting only
    # VEGETATION, NOT_VEGETATED and UNCLASSIFIED. Code values from Figure 3 of 
    # https://sentinel.esa.int/web/sentinel/technical-guides/sentinel-2-msi/level-2a/algorithm
    sclCodesLandSfc = set([4, 5, 7])
    
    numPts = len(pointsXY)
    for i in range(numPts):
        (x, y) = tuple(pointsXY[i])
        sclValue = extractionutils.queryPix(bandObjDictESA, x, y, 'SCL')
        if sclValue in sclCodesLandSfc:
            (sunAz, sunZen) = extractionutils.querySunAngles(granulemeta, x, y)
            pointVals = [x, y, tileId, date, sunAz, sunZen]
            
            allValsOK = True
            for bandIdStr in brdfutils.bandsToProcess:
                refESA = extractionutils.queryPix(bandObjDictESA, x, y, bandIdStr)
                refJRSRP = extractionutils.queryPix(bandObjDictJRSRP, x, y, bandIdStr)

                (satAz, satZen) = extractionutils.querySatAngles(granulemeta, x, y, bandIdStr)
                
                refESAscaled = round(refESA / zipmeta.scaleValue, 4)
                refJRSRPscaled = round(refJRSRP / refScaleJRSRP, 4)
                pointVals.extend([refJRSRPscaled, refESAscaled, satAz, satZen])
                
                refOK = ((refESA != zipmeta.nullVal) and (refJRSRP != refNullJRSRP))
                anglesOK = not (numpy.isnan(satAz) or numpy.isnan(satZen))
                allValsOK = (allValsOK and refOK and anglesOK)

            if allValsOK:
                writeOutputLine(outf, pointVals)


def writeOutputLine(f, dataLine):
    """
    Write a single row of output to the given output file
    """
    print(' '.join([str(v) for v in dataLine]), file=f)


def openAllJRSRPbands(abaname):
    """
    Behaves like extractionutils.openAllBands(), but working with JRSRP
    QVF_named files instead. Returns a dictionary of open band/dataset objects
    and GeoTransform objects, keyed by band name. 
    """
    bandObjDict = {}
    abbname = qvf.changestage(abaname, 'abb')
    
    # Loop over the aba and abb files
    filesAndBands = [
        (abaname, brdfutils.bands10m),
        (abbname, brdfutils.bands20m)
    ]
    for i in range(2):
        (imgname, bandNames) = filesAndBands[i]
        
        ds = gdal.Open(imgname)
        geoTransform = ds.GetGeoTransform()
        for i in range(len(bandNames)):
            bandName = bandNames[i]
            bandObj = ds.GetRasterBand(i+1)
            bandObjDict[bandName] = (ds, bandObj, geoTransform, gdal.InvGeoTransform(geoTransform))

    return bandObjDict


def createOutfile(cmdargs):
    """
    Open a text file (space-separated) for the full output data. Includes a
    line of column headings at the top. 
    """
    # Assemble the column headings. 
    headings = ['Easting', 'Northing', 'Tile', 'Date', 'sunAz', 'sunZen']
    for bandIdStr in brdfutils.bandsToProcess:
        bandHeadings = ["{}_{}".format(bandIdStr, source) for source in ['JRSRP', 'ESA']]
        bandHeadings.extend(['{}_satAz'.format(bandIdStr), '{}_satZen'.format(bandIdStr)])

        headings.extend(bandHeadings)
    
    f = open(cmdargs.outfile, 'w')
    print(' '.join(headings), file=f)
    return f


if __name__ == "__main__":
    main()

