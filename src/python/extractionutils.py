"""
Utility routines for data extraction
"""
from __future__ import print_function, division

import os
import zipfile

import numpy
from osgeo import gdal

from auscophub import sen2meta

import brdfutils

# This translates the given band ID string into the crazy bandId 
# number used within the granule metadata for angles. Most of it is 
# based on the assumption of frequency order, but I am totally 
# guessing that B8A comes after B08. When I get it right, it should 
# be in sen2meta.py
bandId = {
    'B01': '0',
    'B02': '1',
    'B03': '2',
    'B04': '3',
    'B05': '4',
    'B06': '5',
    'B07': '6',
    'B08': '7',
    'B8A': '8',
    'B09': '9',
    'B10': '10',
    'B11': '11',
    'B12': '12'
}


def generatePoints(granulemeta, sampleStep):
    """
    Generate a grid of (x, y) locations across the extent of the given zip file
    (as defined by its granule-level metadata). 
    Does not pay attention to the null data area, just uses the full square extent. 
    We will remove nulls later. 
    The given sampleStep is used as the step between each sample point, in 
    each direction. The first point is in from the edge by half a sampleStep
    The sampleStep is given in metres. 
    Return a numpy array of shape (numPts, 2). The first column is the X coordinate, 
    and the second column is the Y. 
    """
    res = 10
    (nrows, ncols) = granulemeta.dimsByRes[str(res)]
    (ulx, uly) = granulemeta.ulxyByRes[str(res)]
    (lrx, lry) = (ulx + ncols * res, uly - nrows * res)

    xyList = []
    numPtsX = int((lrx - ulx) / sampleStep - 1)
    numPtsY = int((uly - lry) / sampleStep - 1)
    for i in range(numPtsY):
        y = uly - sampleStep / 2 - i * sampleStep
        for j in range(numPtsX):
            x = ulx + sampleStep / 2 + j * sampleStep
            xyList.append((x, y))
    xyArr = numpy.array(xyList)
    return xyArr


def readMetadata(zipfilename):
    """
    Read the two metadata objects from the given zipfile
    Return
        (zipmeta, granulemeta)
    """
    zipmeta = sen2meta.Sen2ZipfileMeta(zipfilename=zipfilename)
    granulemeta = readGranulemeta(zipfilename)
    return (zipmeta, granulemeta)


def openAllBands(zipfilename):
    """
    Open a GDAL band object for each relevant band. Return a dictionary
    keyed by the bandIdStr. Each dictionary value is a tuple of
        (dataSet, bandObj, bandGeotransform, bandInverseGeotransform)
    
    Note that we do NOT use the built-in GDAL SENTINEL2 driver directly. It 
    silently swaps the B02 and B04 bands, which is dreadfully confusing. 
    So instead we use /vsizip/ to open the individual .jp2 files, after 
    searching out their names outselves. 
    
    """
    bandObjDict = {}

    jp2fileEndings = {}
    for bandIdStr in brdfutils.bands10m:
        jp2fileEndings[bandIdStr] = "{}_10m.jp2".format(bandIdStr)
    for bandIdStr in brdfutils.bands20m:
        jp2fileEndings[bandIdStr] = "{}_20m.jp2".format(bandIdStr)
    jp2fileEndings['SCL'] = 'SCL_20m.jp2'

    # The GDAL SENTINEL2 driver does damage by putting the 10m bands in the 
    # wrong order. It appears that B04 comes first, then B03, B02, B08. Since I am 
    # not sure when this occurs, and what other bands might sometimes be affected, 
    # I am not suing it. So, instead I use /vsizip/ to open the individual jp2 files. 
    zf = zipfile.ZipFile(zipfilename, 'r')
    filenames = [zi.filename for zi in zf.infolist()]
    
    for bandIdStr in jp2fileEndings:
        fileEnding = jp2fileEndings[bandIdStr]
        jp2fileList = [fn for fn in filenames if fn.endswith(fileEnding)]
        if len(jp2fileList) != 1:
            raise ExtractionError("Found {} files matching {}".format(len(jp2fieList), fileEnding))
        jp2file = jp2fileList[0]
        
        ds = gdal.Open('/vsizip/{}/{}'.format(zipfilename, jp2file))
        bandObj = ds.GetRasterBand(1)
        geoTransform = ds.GetGeoTransform()
        bandObjDict[bandIdStr] = (ds, bandObj, geoTransform, gdal.InvGeoTransform(geoTransform))

    return bandObjDict


def queryPix(bandObjDict, x, y, bandIdStr):
    """
    Query the pixel value at the given location for the given band. 
    Location is in world coords (i.e. easting/northing). 
    Return a single pixel value for that band. 
    """
    (ds, bandObj, geotransform, invgeotransform) = bandObjDict[bandIdStr]
    (col, row) = gdal.ApplyGeoTransform(invgeotransform, x, y)
    (col, row) = (int(col), int(row))
    pixArr = bandObj.ReadAsArray(col, row, 1, 1)
    pixVal = pixArr[0, 0]
    return pixVal


def readGranulemeta(zipfilename):
    """
    Read the granule-level metadata. The constructor in sen2meta is not
    smart enough to search the zipfile for it, so we have to do that 
    part ourselves. I should put that capability into auscophub.sen2meta, as 
    I have already done for the zip-level metadata. 
    """
    zf = zipfile.ZipFile(zipfilename, 'r')
    filenames = [zi.filename for zi in zf.infolist()]
    safeDirName = [fn for fn in filenames if fn.endswith('.SAFE/')][0]
    granuleSubdir = [fn for fn in filenames if os.path.dirname(fn[:-1]) == (safeDirName+"GRANULE")][0]
    fullmetafilename = [fn for fn in filenames 
        if os.path.dirname(fn) == granuleSubdir[:-1] and fn.endswith("MTD_TL.xml")][0]
    f = zf.open(fullmetafilename)
    granulemeta = sen2meta.Sen2TileMeta(fileobj=f)
    return granulemeta


def querySunAngles(granulemeta, x, y):
    """
    Query the sun angle grid at the given (x, y) location. Return a tuple
        (sunAzimuth, sunZenith)
    with values in degrees. 
    """
    anglesRes = (granulemeta.angleGridXres, granulemeta.angleGridYres)
    anglesULXY = granulemeta.anglesULXY
    sunAz = queryAngleGrid(granulemeta.sunAzimuthGrid, anglesULXY, anglesRes, x, y)
    sunZen = queryAngleGrid(granulemeta.sunZenithGrid, anglesULXY, anglesRes, x, y)
    return (sunAz, sunZen)


def querySatAngles(granulemeta, x, y, bandIdStr):
    """
    Query the satellite angles at the given (x, y) location, for the 
    given band. Return a tuple
        (satAz, satZen)
    with angles in degrees
    """
    satAzGrid = granulemeta.viewAzimuthDict[bandId[bandIdStr]]
    satZenGrid = granulemeta.viewZenithDict[bandId[bandIdStr]]
    anglesRes = (granulemeta.angleGridXres, granulemeta.angleGridYres)
    anglesULXY = granulemeta.anglesULXY
    satAz = queryAngleGrid(satAzGrid, anglesULXY, anglesRes, x, y)
    satZen = queryAngleGrid(satZenGrid, anglesULXY, anglesRes, x, y)
    return (satAz, satZen)


def queryAngleGrid(angleGrid, gridULXY, gridRes, x, y):
    """
    Given the definition of the angle grid in question, return the value found 
    in the grid at the given (x, y) location
    """
    row = int((gridULXY[1] - y) // gridRes[1])
    col = int((x - gridULXY[0]) // gridRes[0])
    angle = angleGrid[row, col]
    return angle


class ExtractionError(Exception): pass
