#!/usr/bin/env python
"""
Given a list of date pairs, use the auscophub client toosl to search the SARA
database for the corresponding Level 2A files. Write a download script to 
retrieve all of them. 

"""
from __future__ import print_function, division

import argparse
import json

from auscophub import saraclient

from rsc.utils import metadb

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--pairfile", help="Pairs text file")
    p.add_argument("--downloadscript", help="Output bash script to download files")
    p.add_argument("--missinglist", help="Output file of pairs which are missing L2A on SARA")
    p.add_argument("--proxy", default="web-espproxy-usr.dmz:80", 
        help="Internet proxy to use")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    dbCon = metadb.connect(api=metadb.DB_API)
    cursor = dbCon.cursor()
    
    pairlist = readPairs(cmdargs.pairfile)
    sceneCentres = getSceneCentres(pairlist, cursor)
    (simpleObjList, missingList) = getSaraObjects(pairlist, sceneCentres, cmdargs)
    if cmdargs.downloadscript is not None:
        writeCurlScript(cmdargs, simpleObjList)
    if cmdargs.missinglist is not None:
        writeMissingList(cmdargs, missingList)


def readPairs(pairfile):
    """
    Read the pairs from the text file. Return a list of the tuples
        [(scene, date1, date2), ....]
    """
    pairlist = []
    f = open(pairfile)
    for line in f:
        words = line.strip().split()
        pairlist.append(tuple(words))
    return pairlist


def getSceneCentres(pairlist, cursor):
    """
    Return a dictionary of the (long, lat) of the centre of each scene.
    """
    sceneCentres = {}
    for (scene, date1, date2) in pairlist:
        if scene not in sceneCentres:
            sceneCentres[scene] = querySceneCentre(scene, cursor)
    return sceneCentres


def querySceneCentre(scene, cursor):
    """
    Query the sentinel2_tiles table to get the scene centre coordinates
    """
    sql = """
        select ST_AsGeoJSON(ST_Centroid(geom_minimal)) 
        from sentinel2_tiles
        where scene = '{}'
    """.format(scene)
    cursor.execute(sql)
    results = cursor.fetchall()
    if len(results) > 0:
        centreJson = results[0][0]
        centreDict = json.loads(centreJson)
        centre = tuple(centreDict['coordinates'])
    return centre


def getSaraObjects(pairlist, sceneCentres, cmdargs):
    """
    Return a list of simple SARA objects for each image, derived from the pairlist
    and the given sceneCentres. 
    """
    simpleObjList = []
    missingList = []
    
    urlOpener = saraclient.makeUrlOpener(cmdargs.proxy)
    sentinel = 2
    for (scene, date1, date2) in pairlist:
        (longitude, latitude) = sceneCentres[scene]
        pair = []
        for date in [date1, date2]:
            paramList = ["startDate={}T00:00:00Z".format(date),
                "completionDate={}T23:59:59Z".format(date),
                "lon={}".format(longitude), "lat={}".format(latitude),
                "processingLevel=L2A"
            ]
            fullJson = saraclient.searchSara(urlOpener, sentinel, paramList)
            if len(fullJson) > 0:
                simpleFeature = saraclient.simplifyFullFeature(fullJson[0])
                pair.append(simpleFeature)

        # If we got both of the pair, then include this in the final list
        if len(pair) == 2:
            simpleObjList.extend(pair)
        else:
            missingList.append((scene, date1, date2))

    return (simpleObjList, missingList)


def writeCurlScript(cmdargs, simpleObjList):
    """
    Write a bash script of curl commands to download all selected files. 
    """
    f = open(cmdargs.downloadscript, 'w')
    f.write("#!/bin/bash\n")
    for simpleObj in simpleObjList:
        url = simpleObj['downloadurl']
        proxyOpt = ""
        if cmdargs.proxy is not None:
            proxyOpt = "-x {}".format(cmdargs.proxy)
        curlCmd = "curl -n -L -O -J {} {}".format(proxyOpt, url)
        f.write(curlCmd+'\n')
    f.close()


def writeMissingList(cmdargs, missingList):
    """
    Write the pairs which were not available on SARA
    """
    f = open(cmdargs.missinglist, 'w')
    for row in missingList:
        f.write(' '.join(row)+'\n')
    f.close()


if __name__ == "__main__":
    main()
