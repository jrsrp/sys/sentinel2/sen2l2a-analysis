#!/usr/bin/env python
"""
Fit BRDF models for each band
"""
from __future__ import print_function, division

import argparse
import json

import numpy
import pandas
import scipy.optimize

import brdfutils

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--satanglepairfile", help=("Input text file of point satellite angle pair data "+
        "(as extracted by extractPairedReflectance.py)"))
    p.add_argument("--sunanglepairfile", help=("Input text file of point sun angle pair data "+
        "(as extracted by extractPairedReflectance.py)"))
    p.add_argument("--paramfile", help="Output parameter file (JSON format)")
    p.add_argument("--mccvoutfile", help="Output file for Monte Carlo Cross Validation results")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    (satPairData, sunPairData) = readAllPointfiles(cmdargs)
    data = pandas.concat([satPairData, sunPairData])
    
    if cmdargs.paramfile is not None:
        allParams = {}
        for bandIdStr in brdfutils.bandsToProcess:
            p = fitParamsForBand(data, bandIdStr)
            allParams[bandIdStr] = {'pGeo':p[0], 'pVol':p[1]}

        writeParams(allParams, cmdargs.paramfile)
    
    if cmdargs.mccvoutfile is not None:
        allMCCV = {}
        for bandIdStr in brdfutils.bandsToProcess:
            allMCCV[bandIdStr] = doCrossValForBand(satPairData, sunPairData, bandIdStr)
        
        writeMCCV(allMCCV, cmdargs.mccvoutfile)


def fitParamsForBand(data, bandIdStr):
    """
    Given a pandas dataframe of the data, and a selected band ID string,
    fit a set of RTLS parameters
    """
    (Kgeo1, Kvol1) = brdfutils.calcBRDFkernelsForBand(data, bandIdStr, 1)
    (Kgeo2, Kvol2) = brdfutils.calcBRDFkernelsForBand(data, bandIdStr, 2)
    ref1 = data['{}_ref1'.format(bandIdStr)].values
    ref2 = data['{}_ref2'.format(bandIdStr)].values
    
    p = numpy.array([0.5, 0.5])
    res = scipy.optimize.minimize(brdfDiff, p, args=(ref1, ref2, Kgeo1, Kvol1, Kgeo2, Kvol2))
    p = res.x
    return p


def brdfDiff(p, ref1, ref2, Kgeo1, Kvol1, Kgeo2, Kvol2):
    """
    Function to be minimized. Calculates the difference in reflectance
    when ref2 is adjusted to the angles for ref1. The parameters p are
    being optimized. 
    
    Returned quantity is the sum of the absolute differences. 
    
    """
    r1 = brdfutils.rtls_n(p, Kgeo1, Kvol1)
    r2 = brdfutils.rtls_n(p, Kgeo2, Kvol2)
    ratio = r1 / r2
    ref2_1 = ref2 * ratio
    diff = numpy.abs(ref1 - ref2_1)
    return diff.sum()
    

def readAllPointfiles(cmdargs):
    """
    Read point files for sunangle and satangle pairs, and return 
    two pandas dataframes
    """
    satPairData = brdfutils.readBRDFpointfile(cmdargs.satanglepairfile)
    sunPairData = brdfutils.readBRDFpointfile(cmdargs.sunanglepairfile)
    return (satPairData, sunPairData)


def writeParams(allParams, paramfile):
    """
    Write the parameters into a suitable file. 
    """
    f = open(paramfile, 'w')
    json.dump(allParams, f, indent=2)
    f.close()


def doCrossValForBand(satPairData, sunPairData, bandIdStr):
    """
    Do cross validation of reflectance adjustment with BRDF model, for 
    the given band
    """
    sunSceneNames = sunPairData['Tile'].unique()
    satSceneNames = satPairData['Tile'].unique()
    
    crossVal_sunPairs = doCrossVal(sunSceneNames, satPairData, sunPairData, bandIdStr)
    crossVal_satPairs = doCrossVal(satSceneNames, sunPairData, satPairData, bandIdStr)
    
    return (crossVal_sunPairs, crossVal_satPairs)


def doCrossVal(sceneNames, nonValidationData, validationData, bandIdStr):
    """
    Do Monte Carlo Cross Validation of BRDF reflectance adjustment. 
    
    The sceneNames list is used to select scenes which are to be used for either 
    fitting or validation, but never both. The two data frames are used to 
    generate fitting and validation data, based on these scene selections, but
    in addition, validation data will only ever be taken from the validationData
    frame, never from the nonValidationData frame. This is what allows us
    to do predictions on the appropriate pairings, e.g. we predict across 
    only swathe overlap pairings, separate from the sun angle pairings, or 
    vice versa. 
    
    Fitting data is taken from both dataframes, but only for the scene names selected
    for fitting in that iteration. 
    
    Return a tuple 
        (madRaw, madAdj)
    Each is a list of Mean Absolute Difference values. First is from the raw reflectance 
    data, while the second uses the BRDF-adjusted reflectance data, where ref2 has been
    adjusted to match ref1. 
    
    """
    numIterations = 100
    numScenes = len(sceneNames)
    numValidationScenes = int(0.3 * numScenes)   # 30% of scenes for validation
    
    # Lists for the Mean Absolute Difference, either of the raw 
    # or the adjusted reflectances
    madRaw = []
    madAdj = []
    
    # Repeat the Monte Carlo process for numInterations
    for i in range(numIterations):
        # Split between training and validation, with whole scenes either one or the other
        sceneNdxRandom = numpy.random.permutation(numScenes)
        validationScenes = sceneNames[sceneNdxRandom[:numValidationScenes]]
        nonValidationScenes = sceneNames[sceneNdxRandom[numValidationScenes:]]
        trainingPts = pandas.concat([
            nonValidationData[nonValidationData['Tile'].isin(nonValidationScenes)],
            validationData[validationData['Tile'].isin(nonValidationScenes)]
        ])
        validationPts = validationData[validationData['Tile'].isin(validationScenes)]
        
        # Fit a model to the training data
        p = fitParamsForBand(trainingPts, bandIdStr)
        
        # Unadjusted reflectances
        ref1 = validationPts['{}_ref1'.format(bandIdStr)]
        ref2 = validationPts['{}_ref2'.format(bandIdStr)]
        mad = numpy.abs(ref1 - ref2).mean()
        madRaw.append(mad)
        
        # Adjust ref2 to match ref1
        (Kgeo1, Kvol1) = brdfutils.calcBRDFkernelsForBand(validationPts, bandIdStr, 1)
        (Kgeo2, Kvol2) = brdfutils.calcBRDFkernelsForBand(validationPts, bandIdStr, 2)
        r1 = brdfutils.rtls_n(p, Kgeo1, Kvol1)
        r2 = brdfutils.rtls_n(p, Kgeo2, Kvol2)
        ref2_1 = ref2 * r1 / r2
        mad = numpy.abs(ref1 - ref2_1).mean()
        madAdj.append(mad)

    return (madRaw, madAdj)


def writeMCCV(allMCCV, mccvoutfile):
    """
    Write a JSON file of the MCCV results
    """
    allStats = {}
    for bandIdStr in allMCCV:
        (crossVal_sunPairs, crossVal_satPairs) = allMCCV[bandIdStr]
        allStats[bandIdStr] = crossValStatsForBand(crossVal_sunPairs, crossVal_satPairs)
    
    json.dump(allStats, open(mccvoutfile, 'w'), indent=2)


def crossValStatsForBand(crossVal_sunPairs, crossVal_satPairs):
    """
    Return a dictionary of the cross validation stats for a single band. 
    Inputs are tuples of (madRaw, madAdj), as returned by doCrossVal. One is
    for validation on the satellite angle pairs, the other is similar but
    for validation on the sun angle pairs. 
    """
    d = {
        'sunPairs_raw': crossValStats(crossVal_sunPairs[0]),
        'sunPairs_adj': crossValStats(crossVal_sunPairs[1]),
        'satPairs_raw': crossValStats(crossVal_satPairs[0]),
        'satPairs_adj': crossValStats(crossVal_satPairs[1])
    }
    return d
    

def crossValStats(madList):
    """
    From the given list of Mean Absolute Difference values, return a tuple of
        (median, lower, upper)
    where the lower and upper values give the 90% confidence interval on MAD. 
    """
    med = numpy.percentile(madList, 50)
    lower = numpy.percentile(madList, 5)
    upper = numpy.percentile(madList, 95)
    return (med, lower, upper)


if __name__ == "__main__":
    main()
