#!/usr/bin/env python
"""
Fit fudges to make ESA reflectance match JRSRP reflectance. 
"""
from __future__ import print_function, division

import argparse
import json

import numpy
from scipy.stats import linregress
import pandas

import brdfutils

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--infile", help="Input file of reflectance point values")
    p.add_argument("--brdfparamfile", help="Parameters to apply to ESA reflectance")
    p.add_argument("--outfile", help="Output JSON file of coefficients for adjustment")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    brdfParams = None
    if cmdargs.brdfparamfile is not None:
        brdfParams = json.load(open(cmdargs.brdfparamfile))
    
    df = pandas.read_csv(cmdargs.infile, delim_whitespace=True)
    
    fitParams = {}
    for bandIdStr in brdfutils.bandsToProcess:
        refJrsrp = df['{}_JRSRP'.format(bandIdStr)]
        refEsa = df['{}_ESA'.format(bandIdStr)]
        if brdfParams is not None:
            refEsa = standardizeRef(refEsa, df, bandIdStr, brdfParams)
        
        reg = linregress(refEsa, refJrsrp)
        regDict = {}
        regDict['slope'] = reg.slope
        regDict['intercept'] = reg.intercept

        fitParams[bandIdStr] = regDict

    json.dump(fitParams, open(cmdargs.outfile, 'w'), indent=2)


def standardizeRef(y, df, bandIdStr, brdfParams):
    """
    Adjust to standard angular configuration
    """
    (Kgeo, Kvol) = brdfutils.calcBRDFkernelsForBand(df, bandIdStr, "")
    Kgeostd = brdfutils.calcKgeo(numpy.radians(45), 0.0, 0.0)
    Kvolstd = brdfutils.calcKvol(numpy.radians(45), 0.0, 0.0)

    params = brdfParams[bandIdStr]
    p = [params['pGeo'], params['pVol']]
    r = brdfutils.rtls_n(p, Kgeo, Kvol)
    rStd = brdfutils.rtls_n(p, Kgeostd, Kvolstd)
    ratio = rStd / r
    
    yAdj = y * ratio
    
    return yAdj


if __name__ == "__main__":
    main()
