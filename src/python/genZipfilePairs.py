#!/usr/bin/env python
"""
Given the list of per-scene date pairs, and a directory full of L2A 
zip files, generate a list of the corresponding pairs of zip file ID strings. 

"""
from __future__ import print_function, division

import os
import argparse
import glob

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--datepairs", help="Text file of per-scene date pairs")
    p.add_argument("--zipfiledir", help="Directory containing zip files")
    p.add_argument("--outfile", help="Output text file of zip file pairs")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    zipfileDict = findZipfiles(cmdargs.zipfiledir)
    
    datepairList = readDatePairs(cmdargs.datepairs)
    
    zipfilePairs = makeZipfilePairs(zipfileDict, datepairList)
    
    f = open(cmdargs.outfile, 'w')
    for (zip1, zip2) in zipfilePairs:
        print(zip1, zip2, file=f)
    f.close()


def makeZipfilePairs(zipfileDict, datepairList):
    """
    Return a list of pairs of zip files
    """
    zipfilePairs = []
    for (scene, date1, date2) in datepairList:
        key1 = (scene.upper(), date1.replace('-', ''))
        key2 = (scene.upper(), date2.replace('-', ''))
        if key1 in zipfileDict and key2 in zipfileDict:
           zip1 = zipfileDict[key1]
           zip2 = zipfileDict[key2]
           zipfilePairs.append((zip1, zip2))
    return zipfilePairs


def findZipfiles(zipfiledir):
    """
    Check the given directory, and return a dictionary of the zip file names
    found there. The dictionary key is a tuple (scene, date), as deduced from the
    filename. 
    """
    pattern = "{}/S2?_MSIL2A_*.zip".format(zipfiledir)
    zipfilelist = glob.glob(pattern)
    
    zipfileDict = {}
    for fn in zipfilelist:
        basename = os.path.basename(fn)
        date = basename[11:19]
        scene = basename[38:44]
        key = (scene, date)
        zipfileDict[key] = fn
    return zipfileDict


def readDatePairs(datepairfile):
    """
    Read the given file for a list of per-scene date pairs. 
    Return a list of triples (scene, date1, date2)
    """
    datepairList = []
    f = open(datepairfile)
    for line in f:
        words = line.strip().split()
        datepairList.append(tuple(words))
    return datepairList


if __name__ == "__main__":
    main()
