#!/usr/bin/env python
"""
Make LaTeX table of the fitted BRDF parameters. 
"""
from __future__ import print_function, division

import argparse
import json

import brdfutils

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--jsonfile", help="Input BRDF parameter JSON file")
    p.add_argument("--latexfile", help="Output LaTeX file of the formatted table")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    results = json.load(open(cmdargs.jsonfile))

    f = open(cmdargs.latexfile, 'w')
    print("\\begin{tabular}{|c|c|c|}", file=f)
    print("\\hline", file=f)
    print("Band&$f^{'}_{geo}$&$f^{'}_{vol}$\\\\", file=f)
    print("\\hline", file=f)
    
    for bandIdStr in brdfutils.bandsToProcess:
        p = results[bandIdStr]
        print("{}&{:.4f}&{:.4f}\\\\".format(bandIdStr, p['pGeo'], p['pVol']), file=f)

    print("\\hline", file=f)
    print("\\end{tabular}", file=f)

if __name__ == "__main__":
    main()
