#!/usr/bin/env python
"""
Make LaTeX table of the MCCV results. 
"""
from __future__ import print_function, division

import argparse
import json

import brdfutils

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--jsonfile", help="Input MCCV JSON file")
    p.add_argument("--latexfile", help="Output LaTeX file of the formatted table")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    results = json.load(open(cmdargs.jsonfile))
    
    f = open(cmdargs.latexfile, 'w')
    print("\\begin{tabular}{|c|c|c|c|c|}", file=f)
    print("\\hline", file=f)
    print("&\\multicolumn{2}{c|}{Swathe Overlaps}&\\multicolumn{2}{c|}{Sun Angle Pairs}\\\\", file=f)
    print("\\hline", file=f)
    print("Band&Unadjusted Diff&Adjusted Diff&Unadjust Diff&Adjusted Diff\\\\", file=f)
    print("\\hline", file=f)
    
    for bandIdStr in brdfutils.bandsToProcess:
        vals = results[bandIdStr]
        
        print("{}&{}&{}&{}&{}\\\\".format(bandIdStr, 
            formatDiffWithConf(vals['satPairs_raw']),
            formatDiffWithConf(vals['satPairs_adj']),
            formatDiffWithConf(vals['sunPairs_raw']),
            formatDiffWithConf(vals['sunPairs_adj'])), 
            file=f)

    print("\\hline", file=f)
    print("\\end{tabular}", file=f)


def formatDiffWithConf(diffStats):
    """
    Input is a list of 3 values, the median and lower and upper bounds. Formats these into 
    a string as "median [lower, upper]", i.e. the difference, with the confidence interval. 
    """
    (med, lower, upper) = tuple(diffStats)
    s = "{:.3f} [{:.3f}, {:.3f}]".format(med, lower, upper)
    return s


if __name__ == "__main__":  
    main()
