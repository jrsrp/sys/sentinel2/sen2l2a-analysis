#!/usr/bin/env python
"""
Create the plots necessary for BRDF assessment. 
"""
from __future__ import print_function, division

import sys
import argparse
import json

import numpy
import pylab

import brdfutils


def getCmdargs():
    """
    Get command line arguments
    """
    orderingList = [brdfutils.ORDER_SATAZ, brdfutils.ORDER_SUNZEN, brdfutils.ORDER_UNCHANGED]
    
    p = argparse.ArgumentParser()
    p.add_argument("--refpairsfile", help="Text file of paired reflectance values and angles")
    p.add_argument("--plotfile_tmbands", help="Output plot file for the 6 TM-like bands")
    p.add_argument("--plotfile_rededge", help="Output plot file for the 4 red-edge bands")
    p.add_argument("--plotfile_anglediff", help="Output plotfile for angle difference histogram")
    p.add_argument("--ordering", choices=orderingList, 
        help="Choose which variable is used to order point pairs")
    p.add_argument("--brdfparamfile", help=("Input BRDF param file (JSON). If given, then "+
        "reflectance obs 2 will be adjusted (using the params) to match reflectance obs 1, "+
        "before plotting. Default does not adjust. "))
    cmdargs = p.parse_args()
    
    if cmdargs.ordering is None:
        print("--ordering is required")
        sys.exit()

    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    data = brdfutils.readBRDFpointfile(cmdargs.refpairsfile)
    brdfParams = None
    if cmdargs.brdfparamfile is not None:
        brdfParams = json.load(open(cmdargs.brdfparamfile))
    
    tmlikeBandList = ['B02', 'B03', 'B04', 'B08', 'B11', 'B12']
    rededgeBandList = ['B05', 'B06', 'B07', 'B8A']
    
    tmlikeRefPairs = assemblePairedData(tmlikeBandList, data, cmdargs.ordering, brdfParams)
    brdfutils.plotScatterGroup(tmlikeRefPairs, tmlikeBandList, cmdargs.plotfile_tmbands)

    rededgeRefPairs = assemblePairedData(rededgeBandList, data, cmdargs.ordering, brdfParams)
    brdfutils.plotScatterGroup(rededgeRefPairs, rededgeBandList, cmdargs.plotfile_rededge)
    
    plotAngleDiff(data, 'B02', cmdargs)
    
    
def assemblePairedData(bandList, data, orderBy, brdfParams):
    """
    Return a list of pairs of reflectance arrays. The ordering of each point
    has been modified according to the orderBy parameter, which can have values
    from the symbols brdfutils.ORDER_*. 
    
    If brdfParams is not None, then these params are used to adjust the reflectance, 
    such that ref2 is adjusted to match ref1. 
    
    """
    refDataList = []
    if orderBy == brdfutils.ORDER_SUNZEN:
        # Include only points on the eastern side of the swathe, to avoid 
        # dilution of sun angle effects
        easternSide = ((data['B02_satAz1'] > 180) & (data['B02_satZen1'] > 8))
        data = data[easternSide]
        
        # X axis will be from lower sun elevation, i.e. larger sun zenith
        orderFlag = numpy.where((data["sunZen1"].values > data["sunZen2"].values), 0, 1)
    elif orderBy == brdfutils.ORDER_UNCHANGED:
        orderFlag = numpy.zeros(len(data), dtype=numpy.bool)

    for bandIdStr in bandList:
        ref1 = data["{}_ref1".format(bandIdStr)].values
        ref2 = data["{}_ref2".format(bandIdStr)].values
        
        if brdfParams is not None:
            ref2 = brdfAdjust2to1(ref2, data, brdfParams, bandIdStr)
        
        if orderBy == brdfutils.ORDER_SATAZ:
            # X axis will be from eastern edge of swathe
            orderFlag = numpy.where((data["{}_satAz1".format(bandIdStr)].values > 180.0), 0, 1)
        refPair = numpy.vstack((ref1, ref2)).T
        if orderBy != brdfutils.ORDER_UNCHANGED:
            refPair = brdfutils.orderRefBySelector(refPair, orderFlag)
        (x, y) = (refPair[:, 0], refPair[:, 1])
        refDataList.append((x, y))

    return refDataList


def plotAngleDiff(data, bandIdStr, cmdargs):
    """
    Make a histogram of the angle difference between the paired observations. 
    The angle used depends on cmdargs.ordering. 
    """
    if cmdargs.plotfile_anglediff is not None:
        pylab.figure(figsize=(4, 4))
        pylab.subplots_adjust(left=0.15, bottom=0.12, right=0.98, top=0.97)

        if cmdargs.ordering == brdfutils.ORDER_SUNZEN:
            angleDiff = numpy.abs(data['sunZen1'] - data['sunZen2'])
            pylab.xlabel("Sun Zenith Difference (degrees)")
        elif cmdargs.ordering == brdfutils.ORDER_SATAZ:
            # Note that we ADD the two satellite zenith angles, because they
            # come from opposite sides, and are therefore already in opposite directions. 
            angleDiff = (data['{}_satZen1'.format(bandIdStr)] + 
                data['{}_satZen2'.format(bandIdStr)])
            pylab.xlabel("Satellite Zenith Difference (degrees)")
    
        pylab.hist(angleDiff, bins=range(25))
        pylab.savefig(cmdargs.plotfile_anglediff)


def brdfAdjust2to1(ref2, data, brdfParams, bandIdStr):
    """
    Use the angles in data to adjust the given ref2 to match ref1. Return a single 
    ref2 array
    """
    (Kgeo1, Kvol1) = brdfutils.calcBRDFkernelsForBand(data, bandIdStr, 1)
    (Kgeo2, Kvol2) = brdfutils.calcBRDFkernelsForBand(data, bandIdStr, 2)

    # Extract the right set of parameters
    pDict = brdfParams[bandIdStr]
    p = [pDict['pGeo'], pDict['pVol']]

    r1 = brdfutils.rtls_n(p, Kgeo1, Kvol1)
    r2 = brdfutils.rtls_n(p, Kgeo2, Kvol2)
    ref2_1 = ref2 * r1 / r2

    return ref2_1


if __name__ == "__main__":
    main()
