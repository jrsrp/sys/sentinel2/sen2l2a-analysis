#!/usr/bin/env python
"""
Plot comparisons of reflectance values between ESA L2A and JRSRP surface reflectance. 

"""
from __future__ import print_function, division

import argparse
import json

import numpy
import pandas

import brdfutils

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--infile", help="Extracted point data")
    p.add_argument("--brdfparamfile", help="Parameters to apply to ESA reflectance")
    p.add_argument("--jrsrpfudge", 
        help="Parameter file for linear fudge to make ESA match JRSRP reflectance")
    p.add_argument("--plotfile_tmlike")
    p.add_argument("--plotfile_rededge")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    df = pandas.read_csv(cmdargs.infile, delim_whitespace=True)
    brdfParams = None
    if cmdargs.brdfparamfile is not None:
        brdfParams = json.load(open(cmdargs.brdfparamfile))
    fudgeParams = None
    if cmdargs.jrsrpfudge is not None:
        fudgeParams = json.load(open(cmdargs.jrsrpfudge))
    
    bandNamesTM = ['B02', 'B03', 'B04', 'B08', 'B11', 'B12']
    bandNamesRedEdge = ['B05', 'B06', 'B07', 'B8A']
    if cmdargs.plotfile_tmlike is not None:
        makePlotGroup(bandNamesTM, df, brdfParams, fudgeParams, cmdargs.plotfile_tmlike)

    if cmdargs.plotfile_rededge is not None:
        makePlotGroup(bandNamesRedEdge, df, brdfParams, fudgeParams, cmdargs.plotfile_rededge)


def makePlotGroup(bandNames, df, brdfParams, fudgeParams, plotfile):
    """
    Make a single plot group of the given set of bands
    """
    xyList = []
    for bandIdStr in bandNames:
        x = df["{}_JRSRP".format(bandIdStr)]
        y = df["{}_ESA".format(bandIdStr)]
        
        if brdfParams is not None:
            y = standardizeRef(y, df, bandIdStr, brdfParams)
        if fudgeParams is not None:
            y = jrsrpFudge(y, bandIdStr, fudgeParams)

        xyList.append((x, y))

    brdfutils.plotScatterGroup(xyList, bandNames, plotfile)
    

def standardizeRef(y, df, bandIdStr, brdfParams):
    """
    Adjust to standard angular configuration
    """
    (Kgeo, Kvol) = brdfutils.calcBRDFkernelsForBand(df, bandIdStr, "")
    Kgeostd = brdfutils.calcKgeo(numpy.radians(45), 0.0, 0.0)
    Kvolstd = brdfutils.calcKvol(numpy.radians(45), 0.0, 0.0)

    params = brdfParams[bandIdStr]
    p = [params['pGeo'], params['pVol']]
    r = brdfutils.rtls_n(p, Kgeo, Kvol)
    rStd = brdfutils.rtls_n(p, Kgeostd, Kvolstd)
    ratio = rStd / r
    
    yAdj = y * ratio
    
    return yAdj


def jrsrpFudge(y, bandIdStr, fudgeParams):
    """
    Apply the reflectance fudge
    """
    slope = fudgeParams[bandIdStr]['slope']
    intercept = fudgeParams[bandIdStr]['intercept']
    
    yFudged = y * slope + intercept
    
    return yFudged


if __name__ == "__main__":
    main()
