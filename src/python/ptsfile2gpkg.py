#!/usr/bin/env python
"""
Take a points file (ASCII text) and write a corresponding GPKG file. Keep
all the values as attributes (not sure why, but just in case). 

"""
from __future__ import print_function, division

import argparse

from osgeo import ogr, osr

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--infile", help="Input text points file (written by extractPairedReflectance.py)")
    p.add_argument("--gpkgfile", help="Output GPKG file")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    f = open(cmdargs.infile)
    headings = f.readline().strip().split()
    (ds, lyr) = createGPKGfile(cmdargs, headings)
    
    for line in f:
        addFeature(line, headings, lyr)
    
    del lyr
    del ds


stringFields = ['Tile', 'Date1', 'Date2', 'Date']
def createGPKGfile(cmdargs, headings):
    """
    Create a new GPKG file, with attributes for all the given headings. 
    """
    drvr = ogr.GetDriverByName('GPKG')
    ds = drvr.CreateDataSource(cmdargs.gpkgfile)
    
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)
    lyr = ds.CreateLayer('pts', srs=srs, geom_type=ogr.wkbPoint)
    
    for colname in headings:
        fieldType = ogr.OFTReal
        width = 10
        if colname in stringFields:
            fieldType = ogr.OFTString
        fieldDefn = ogr.FieldDefn(colname, fieldType)
        fieldDefn.SetWidth(width)
        if fieldType == ogr.OFTReal:
            fieldDefn.SetPrecision(4)
        lyr.CreateField(fieldDefn)
    
    return (ds, lyr)


def addFeature(line, headings, lyr):
    """
    Add a new feature to the layer
    """
    featDefn = lyr.GetLayerDefn()
    feat = ogr.Feature(featDefn)
    
    words = line.strip().split()
    numCols = len(headings)
    x = None
    y = None
    zone = None
    for i in range(numCols):
        colname = headings[i]
        if colname in stringFields:
            value = words[i]
        else:
            value = float(words[i])
        feat[colname] = value
        if colname == 'Easting':
            x = value
        elif colname == 'Northing':
            y = value
        elif colname == 'Tile':
            zone = int(value[1:3])
    
    srUTM = osr.SpatialReference()
    srUTM.ImportFromEPSG(32700+zone)
    srLL = lyr.GetSpatialRef()
    # Cope with GDAL 3 reversal of lat/long
    if hasattr(srLL, 'SetAxisMappingStrategy'):
        srLL.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    tr = osr.CoordinateTransformation(srUTM, srLL)
    (longitude, latitude, z) = tr.TransformPoint(x, y)
    geom = ogr.Geometry(wkt='POINT({} {})'.format(longitude, latitude))
    feat.SetGeometry(geom)
    
    lyr.CreateFeature(feat)


if __name__ == "__main__":
    main()

