#!/usr/bin/env python
"""
Given a JSON file of SARA query results, and access to the RSC database,
select a set of Sentinel-2 tiles from across Australia (or at least the JRSRP region 
of interest). 

For each tile, one S2A and one S2B date are selected from those dates which 
are listed as cloud free in both the RSC database and the SARA JSON output. 

"""
from __future__ import print_function, division

import argparse

import numpy
from osgeo import ogr

import qv
from rsc.utils import metadb

import selectionutils

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--sararesults", help="JSON file of SARA results")
    p.add_argument("--proxy", help="Internet proxy to use in download script")
    p.add_argument("--pairfile", help="Output text file of zip/QVF file pairs")
    p.add_argument("--downloadscript", 
        help="Bash script of curl commands to download all zip files")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    rscHoldings = getRSCholdings()
    print('rscHoldings', len(rscHoldings))
    commonDates = getCommonDates(cmdargs, rscHoldings)
    print('commonDates', len(commonDates))
    
    selectedDates = selectPerSceneSat(commonDates)
    print('selectedDates', len(selectedDates))
    
    writeOutfiles(cmdargs, selectedDates)


def getRSCholdings():
    """
    Query sentinel2_list table. Return a set(), of tuples
        (qvf_sat, scene, date)
    """
    dbCon = metadb.connect(api=metadb.DB_API)
    cursor = dbCon.cursor()
    
    sql = """
        select qvf_sat, scene, date from sentinel2_list 
        join cloudamount using (scene, date)
        where pcntfull > 90 and pcntcloud_land < 1
    """
    cursor.execute(sql)
    results = cursor.fetchall()
    
    rscHoldings = set([tuple(r) for r in results])
    
    del cursor
    dbCon.close()
    
    return rscHoldings


def getCommonDates(cmdargs, rscHoldings):
    """
    Go through all the dates from the SARA results, match with those in the rscHoldings
    set(), and return a dictionary of dates which are common to both. 
    The dictionary is keyed by (scene, qvf_sat), and each entry is a list (in order)
    of the dates in common. Each list element is a dictionary of a few key items of interest
    for each date. 
    
    """
    ds = ogr.Open(cmdargs.sararesults)
    lyr = ds.GetLayer()
    
    filteredFeatures = selectionutils.removeSmall(lyr, 90)
    qvfSatDict = {"S2A": "ce", "S2B": "cf"}
    
    commonDates = {}
    for feat in filteredFeatures:
        scene = feat['scene']
        sat = feat['esaid'][:3]
        date = feat['acqdateutc'].strftime("%Y%m%d")
        qvfSat = qvfSatDict[sat]
        idTuple = (qvfSat, scene.lower(), date)
        if idTuple in rscHoldings:
            outKey = (scene.lower(), qvfSat)
            if outKey not in commonDates:
                commonDates[outKey] = []
            commonDates[outKey].append(feat)
    
    for outKey in commonDates:
        featList = commonDates[outKey]
        commonDates[outKey] = sorted(featList, key=lambda x: x['acqdateutc'])
    
    return commonDates


def selectPerSceneSat(commonDates):
    """
    For each (scene, sat), select the most recent date available
    Return a dictionary like commonDates, but with only the selected dates. 
    Select the earliest and latest available for each. 
    """
    selectedDates = {}
    for key in commonDates:
        numDates = len(commonDates[key])
        randomNdx = numpy.random.randint(numDates)
        selectedDates[key] = [commonDates[key][randomNdx]]
    return selectedDates


def writeOutfiles(cmdargs, selectedDates):
    """
    Write all the output files
    """
    # Write the download script for the zip files
    proxyOpt = ""
    if cmdargs.proxy is not None:
        proxyOpt = "-x {}".format(cmdargs.proxy)

    keyList = sorted(selectedDates.keys())

    fPairs = open(cmdargs.pairfile, 'w')
    fDownload = open(cmdargs.downloadscript, 'w')
    fDownload.write("#!/bin/bash\n")
    for (scene, qvfSat) in keyList:
        for feat in selectedDates[(scene, qvfSat)]:
            date = feat['acqdateutc'].strftime("%Y%m%d")
            qvfname = "{}msre_{}_{}_abamz.img".format(qvfSat, scene, date)
            qvfname = metadb.stdProjFilename(qvfname)
            if qv.existsonfilestore(qvfname):
                esaid = feat['esaid']
                fPairs.write("{} {}.zip\n".format(qvfname, esaid))

                downloadUrl = feat['downloadurl']

                curlCmd = "curl -n -L -O -J {} {}".format(proxyOpt, downloadUrl)
                fDownload.write(curlCmd+'\n')

    fPairs.close()
    fDownload.close()      


if __name__ == "__main__":
    main()
