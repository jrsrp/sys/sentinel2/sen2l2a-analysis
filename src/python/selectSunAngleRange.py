#!/usr/bin/env python
"""
Use RSC database tables to select pairs of dates which have a wide difference
in sun zenith angle. Pairs are chosen on opposite sides of the equinox(es), 
separated by 5-7 weeks. This typically gives a sun zenith difference of 
around 15 degrees. Both spring and autumn equinoxes are used. 

"""
from __future__ import print_function, division

import argparse
import datetime
import math

from osgeo import ogr

import selectionutils

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--sararesults", help="GeoJSON file of SARA search results")
    p.add_argument("--maxperscene", type=int, default=6,
        help="Maximum number of date pairs to select per scene (default=%(default)s)")
    p.add_argument("--proxy", help="Internet proxy to use in download script")
    p.add_argument("--zippairs", help="Output text file of zip file pairs")
    p.add_argument("--downloadscript", 
        help="Bash script of curl commands to download all zip files")
    p.add_argument("--existingzipsdir", help=("Directory in which some zip files already exists. "+
        "Any files which are already present will not be included in the download script. "+
        "They will still be included in the zippairs output. "))
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    pairDict = selectPairs(cmdargs)
    pairDict = selectSubsample(pairDict, cmdargs)
    seasonCount(pairDict)
    selectionutils.reportCounts(pairDict)
    selectionutils.writePairs(pairDict, cmdargs)
    
    
def selectPairs(cmdargs):
    """
    Select pairs of Sentinel-2 image dates which are on the same tile and 
    relative orbit, but from dates roughly 3 weeks on either side ofthe 
    spring or autumn equinox.  This maximizes the sun angle difference while 
    minimizing the chance of on-ground change (although 6 weeks is longer than
    I would like). 
    
    """
    datasource = ogr.Open(cmdargs.sararesults)
    lyr = datasource.GetLayer()
    
    # Filter out those which are "small". SARA does not have a field for this, but on
    # the RSC database I would use something like "pcntfull>50"
    featureList = selectionutils.removeSmall(lyr, 50)

    # Restrict to those dates in the period on either side of the equinoxes. Use a 
    # dictionary to group by (tile, relOrbit)
    datesBySceneOrbit = {}
    longPeriod = datetime.timedelta(days=42)    # 6 week period
    shortPeriod = 7                             # 7 days
    for feat in featureList:
        if (closeToMonthDay(feat['acqdateutc'], 3, 1, shortPeriod) or 
                closeToMonthDay(feat['acqdateutc'], 4, 11, shortPeriod) or
                closeToMonthDay(feat['acqdateutc'], 9, 1, shortPeriod) or 
                closeToMonthDay(feat['acqdateutc'], 10, 12, shortPeriod)):
            key = (feat['scene'], feat['relative_orbit'])
            if key not in datesBySceneOrbit:
                datesBySceneOrbit[key] = []
            datesBySceneOrbit[key].append(feat)
    
    # Now find pairs which are separated by between 5 and 7 weeks
    pairDict = {}
    for key in datesBySceneOrbit:
        datelist = sorted(datesBySceneOrbit[key], key=lambda p: p['acqdateutc'])
        nDates = len(datelist)
        for i in range(nDates):
            feat1 = datelist[i]
            for j in range(i+1, nDates):
                feat2 = datelist[j]
                if abs((feat1['acqdateutc'] + longPeriod) - feat2['acqdateutc']).days <= shortPeriod:
                    (scene, relOrbit) = key
                    if scene not in pairDict:
                        pairDict[scene] = []
                    pairDict[scene].append((feat1, feat2))
    
    return pairDict


def seasonCount(pairDict):
    """
    Some reporting, just so I know what I have. Just prints to the screen. 
    """
    springCount = 0
    autumnCount = 0
    for k in pairDict:
        for p in pairDict[k]:
            if p[0]['acqdateutc'].month > 6:
                springCount += 1
            else:
                autumnCount += 1
    print('Spring:', springCount, 'pairs')
    print('Autumn:', autumnCount, 'pairs')


def closeToMonthDay(date, month, day, maxDays):
    """
    Returns True if the date object given is within maxDays of the given 
    month/day of the same year. 
    """
    date2 = datetime.date(date.year, month, day)
    daysDiff = abs(date - date2).days
    return (daysDiff <= maxDays)


def selectSubsample(pairDict, cmdargs):
    """
    Sub-sample by scene. Select only a sub-sample of the scenes, and the date pairs 
    for each scene, to include in the final output. Ensure a balance of Spring and Autumn
    pairs for each scene. 
    """
    pairDict2 = {}
    sceneList = list(pairDict.keys())
    numScenes = len(sceneList)
    for i in range(0, numScenes):
        scene = sceneList[i]
        datelist = pairDict[scene]
        springPairs = [pair for pair in datelist if pair[0]['acqdateutc'].month >= 7]
        autumnPairs = [pair for pair in datelist if pair[0]['acqdateutc'].month < 7]
        maxdates = min(len(springPairs), len(autumnPairs))
        maxdates = min(maxdates, cmdargs.maxperscene//2)
        if maxdates > 0:
            datelistSubset = (subsetDatepairList(springPairs, maxdates) +
                subsetDatepairList(autumnPairs, maxdates))
            pairDict2[scene] = datelistSubset
    return pairDict2


def subsetDatepairList(datepairlist, maxdates):
    """
    Select a maximum of maxdates entries from the given datepairlist. Return 
    an equivalent list
    """
    numDates = len(datepairlist)
    dateStep = int(math.ceil(numDates / maxdates))
    dateStep = max(dateStep, 1)
    datelistSubset = [datepairlist[i] for i in range(0, numDates, dateStep)]
    return datelistSubset


if __name__ == "__main__":
    main()
