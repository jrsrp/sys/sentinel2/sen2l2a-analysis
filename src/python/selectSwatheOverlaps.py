#!/usr/bin/env python
"""
Use the SARA to select a set of Sentinel-2 image pairs which 
overlap on swathe edges. Restricted to the given date range, with the default
earliest date being the start of the available L2A data from ESA. 

"""
from __future__ import print_function, division

import argparse
import datetime

from osgeo import ogr

import selectionutils

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--sararesults", help="GeoJSON file of SARA search results")
    p.add_argument("--maxperscene", type=int, default=4, help=("Maximum number of pairs "+
        "for each scene (randomly chosen from available) (default=%(default)s)"))
    p.add_argument("--minperscene", type=int, default=20,
        help="Minimum number of date pairs required for a scene to be included (default=%(default)s)")
    p.add_argument("--proxy", help="Internet proxy to use in download script")
    p.add_argument("--zippairs", help="Output text file of zip file pairs")
    p.add_argument("--downloadscript", 
        help="Bash script of curl commands to download all zip files")
    p.add_argument("--existingzipsdir", help=("Directory in which some zip files already exists. "+
        "Any files which are already present will not be included in the download script. "+
        "They will still be included in the zippairs output. "))
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    
    pairDict = selectPairs(cmdargs)
    pairDict = selectSubsample(pairDict, cmdargs)
    selectionutils.reportCounts(pairDict)
    selectionutils.writePairs(pairDict, cmdargs)


def selectPairs(cmdargs):
    """
    Select pairs of Sentinel-2 image dates which overlap, but are from different
    relative orbits. 
    
    """
    datasource = ogr.Open(cmdargs.sararesults)
    lyr = datasource.GetLayer()
    
    # Filter out those which are "small". SARA does not have a field for this, but on
    # the RSC database I would use something like "pcntfull>40"
    featureList = selectionutils.removeSmall(lyr, 40)
    
    # Split by scene, and relative_orbit
    byScene = {}
    for feat in featureList:
        scene = feat['scene']
        if scene not in byScene:
            byScene[scene] = {}

        byOrbit = byScene[scene]
        relative_orbit = feat['relative_orbit']
        if relative_orbit not in byOrbit:
            byOrbit[relative_orbit] = []
        
        byOrbit[relative_orbit].append(feat)
    
    # Select pairs which have different relative orbit for the same scene, and dates 
    # which are separated by a short time interval. 3 days is enough to allow
    # pairs which are from the same satellite, not just those from differing satellites. 
    interval = datetime.timedelta(days=3.5)
    pairDict = {}
    for scene in byScene:
        byOrbit = byScene[scene]
        orbitList = list(byOrbit.keys())
        if len(orbitList) == 2:
            (orbit1, orbit2) = tuple(orbitList)
            for r1 in byOrbit[orbit1]:
                for r2 in byOrbit[orbit2]:
                    if abs(r1['acqdateutc'] - r2['acqdateutc']) <= interval:
                        if scene not in pairDict:
                            pairDict[scene] = []
                        pairDict[scene].append((r1, r2))
    
    return pairDict


def selectSubsample(pairDict, cmdargs):
    """
    Select an even sampling of date pairs for each scene, up to 
    cmdargs.maxperscene
    
    """
    pairDict2 = {}
    for scene in pairDict:
        # Sort into chronological order
        pairList = sorted(pairDict[scene], key=lambda p: p[0]['acqdateutc'])
        
        numPairs = len(pairList)
        if numPairs >= cmdargs.minperscene:
            step = numPairs // cmdargs.maxperscene
            # Subsample evenly through the time sequence
            pairDict2[scene] = [pairList[i] for i in range(0, numPairs, step)]
        
    return pairDict2


if __name__ == "__main__":
    main()

