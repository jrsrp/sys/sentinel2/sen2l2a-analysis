"""
Functions common to the selection scripts. 
"""
from __future__ import print_function, division

import os
import json
import datetime

from osgeo import osr

def writePairs(pairDict, cmdargs):
    """
    Write a text file of the selected pairs
    """
    f = open(cmdargs.zippairs, 'w')
    sceneList = sorted(pairDict.keys())
    downloadUrlList = []
    for scene in sceneList:
        pairList = pairDict[scene]
        for (r1, r2) in pairList:
            zip1 = r1['esaid'] + ".zip"
            zip2 = r2['esaid'] + ".zip"
            f.write("{} {}\n".format(zip1, zip2))

            # Add the download URL to the list, but only if the zipfile
            # is not already present in cmdargs.existingzipsdir            
            for i in range(2):
                zipname = [zip1, zip2][i]
                downloadUrl = [r1, r2][i]['downloadurl']
                if cmdargs.existingzipsdir is None or not os.path.exists(os.path.join(cmdargs.existingzipsdir, zipname)):
                    downloadUrlList.append(downloadUrl)
    f.close()

    # Remove duplicates
    downloadUrlList = list(set(downloadUrlList))

    proxyOpt = ""
    if cmdargs.proxy is not None:
        proxyOpt = "-x {}".format(cmdargs.proxy)
    f = open(cmdargs.downloadscript, 'w')
    f.write("#!/bin/bash\n")
    for downloadUrl in downloadUrlList:
        curlCmd = "curl -n -L -O -J {} {}".format(proxyOpt, downloadUrl)
        f.write(curlCmd+'\n')
    f.close()


def removeSmall(lyr, minPcnt):
    """
    Remove features from the given list if they are "small", where this is defined
    as having an area < (minPcnt * 100km*100km) i.e. smaller than minPcnt of a standard 
    Sentinel-2 tile. Does not really account for the extra 10km tacked on to the 
    east and south of each tile, but this is roughly good enough. 
    
    Returns a list of the selected items. Each list element is a dictionary of
    the bits we need for subsequent steps 
    """
    # First work out the spatial reference of the polygons, then a transformation to
    # an equal area projection, so I can get the coordinates in metres. 
    srLL = lyr.GetSpatialRef()
    srAlbers = osr.SpatialReference()
    srAlbers.ImportFromEPSG(3577)
    tr = osr.CoordinateTransformation(srLL, srAlbers)
    
    filteredFeatures = []
    for feat in lyr:
        geom = feat.GetGeometryRef()
        # Project to Albers Equal Area
        geom.Transform(tr)
        area = geom.Area()
        areaPcnt = 100 * area / (100000**2)
        if areaPcnt > minPcnt:
            d = {}
            esaid = feat['productIdentifier']
            d['esaid'] = esaid
            servicesDict = json.loads(feat['services'])
            d['downloadurl'] = servicesDict['download']['url']
            # Add some other things, deduced from the ESA ID string
            d['scene'] = esaid[38:44]
            d['relative_orbit'] = esaid[34:37]
            (year, month, day) = (int(esaid[11:15]), int(esaid[15:17]), int(esaid[17:19]))
            d['acqdateutc'] = datetime.date(year, month, day)
            filteredFeatures.append(d)

    return filteredFeatures


def reportCounts(pairDict):
    """
    Report how many scenes and pairs are to be included. 
    """
    sceneList = list(pairDict.keys())
    numPairs = sum([len(pairDict[scene]) for scene in sceneList])
    avgPairsPerScene = numPairs / len(sceneList)
    print("Found {} scenes, total {} pairs, average {:.1f} pairs/scene".format(
        len(sceneList), numPairs, avgPairsPerScene))


