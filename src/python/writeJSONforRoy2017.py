#!/usr/bin/env python
"""
Convert the BRDF parameters from Roy et al (2017) to the same 2-parameter form
as I am using here, and save as a JSON file. 

Takes an input file of an existing set of parameters, so that we can salvage 
our own values for the red edge bands, which Roy does nt provide. For the other 
bands, our parameters are replaced with those derived from Roy. 
"""
from __future__ import print_function, division

import argparse
import json

import numpy

def getCmdargs():
    """
    Get command line arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("--outfile", help="Output JSON file of parameters")
    p.add_argument("--inparams", help="Input JSON of our own parameters")
    cmdargs = p.parse_args()
    return cmdargs


def main():
    """
    Main routine
    """
    cmdargs = getCmdargs()
    # Load existing params
    pDict = json.load(open(cmdargs.inparams))
    
    # Copied from Roy et al (2017), table 1. 
    pRoy = numpy.array([
        [0.0774, 0.0079, 0.0372],
        [0.1306, 0.0178, 0.0580],
        [0.1690, 0.0227, 0.0574],
        [0.3093, 0.0330, 0.1535],
        [0.3430, 0.0453, 0.1154],
        [0.2658, 0.0387, 0.0639],
    ])
    bandNames = ['B02', 'B03', 'B04', 'B08', 'B11', 'B12']
    
    for i in range(len(bandNames)):
        # Create normalized parameters, by dividing by Fiso
        p = {
            'pGeo': pRoy[i, 1] / pRoy[i, 0],
            'pVol': pRoy[i, 2] / pRoy[i, 0]
        }
        pDict[bandNames[i]] = p
        
        json.dump(pDict, open(cmdargs.outfile, 'w'), indent=2)


if __name__ == "__main__":
    main()

