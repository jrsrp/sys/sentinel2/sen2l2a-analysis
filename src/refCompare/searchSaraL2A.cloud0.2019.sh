#!/bin/bash

# Run a huge SARA query for all available Sentinel-2 L2A images over Australia.
# This takes hours, but the result is saved locally in a GeoJSON file,
# which can then be used quickly. 
# The rough region-of-interest for Australia is present as 
# the GeoJSON file aus.json. 

auscophub_searchSara.py --sentinel 2 --polygonfile ../brdfEffects/aus.json \
    --proxy $RSC_BULKDOWNLOAD_REQUIREDPROXY \
    -q "processingLevel=L2A" -q "cloudCover=0" \
    -q "startDate=2018-12-01T00:00:00" -q "completionDate=2019-08-31T23:59:59" \
    --jsonfeaturesfile ausLevel2A_cloud0_2019.json
